#include "Board.h"
#include "eval.h"
#include "movegen.h"

Board::Board()
{
}

Board::~Board()
{
}

std::string Board::dump_all_bb()
{
    std::string str = "";
    
    str.append("WHITE\n");
    str.append(Bitboards::ascii(m_white_bb));
    str.append("\n");
    
    str.append("BLACK\n");
    str.append(Bitboards::ascii(m_black_bb));
    str.append("\n");
    
    str.append("EMPTY\n");
    str.append(Bitboards::ascii(m_empty_bb));
    str.append("\n");
    
    str.append("OCCUPIED\n");
    str.append(Bitboards::ascii(m_occupied_bb));
    str.append("\n");
    
    str.append("W_PAWN\n");
    str.append(Bitboards::ascii(m_piece_bb[W_PAWN]));
    str.append("\n");
    
    str.append("W_KNIGHT\n");
    str.append(Bitboards::ascii(m_piece_bb[W_KNIGHT]));
    str.append("\n");
    
    str.append("W_BISHOP\n");
    str.append(Bitboards::ascii(m_piece_bb[W_BISHOP]));
    str.append("\n");
    
    str.append("W_ROOK\n");
    str.append(Bitboards::ascii(m_piece_bb[W_ROOK]));
    str.append("\n");
    
    str.append("W_QUEEN\n");
    str.append(Bitboards::ascii(m_piece_bb[W_QUEEN]));
    str.append("\n");
    
    str.append("W_KING\n");
    str.append(Bitboards::ascii(m_piece_bb[W_KING]));
    str.append("\n");
    
    str.append("B_PAWN\n");
    str.append(Bitboards::ascii(m_piece_bb[B_PAWN]));
    str.append("\n");
    
    str.append("B_KNIGHT\n");
    str.append(Bitboards::ascii(m_piece_bb[B_KNIGHT]));
    str.append("\n");
    
    str.append("B_BISHOP\n");
    str.append(Bitboards::ascii(m_piece_bb[B_BISHOP]));
    str.append("\n");
    
    str.append("B_ROOK\n");
    str.append(Bitboards::ascii(m_piece_bb[B_ROOK]));
    str.append("\n");
    
    str.append("B_QUEEN\n");
    str.append(Bitboards::ascii(m_piece_bb[B_QUEEN]));
    str.append("\n");
    
    str.append("B_KING\n");
    str.append(Bitboards::ascii(m_piece_bb[B_KING]));
    str.append("\n");
    
    str.append("EMPTY (piece)\n");
    str.append(Bitboards::ascii(m_piece_bb[EMPTY]));
    str.append("\n");
    
    str.append("\n");
    
    return str;
}

std::string Board::fen() const
{
    std::string fen = "";
    for (Rank rank = RANK_8; rank >= RANK_1; --rank)
    {
        int empty = 0;
        for (File file = FILE_A; file <= FILE_H; ++file)
        {
            Square square = Squares::make(file, rank);
            Piece piece = static_cast<Piece>(m_square_bb[square]);
            if (piece != EMPTY)
            {
                if (empty > 0) 
                {
                    fen.append(std::to_string(empty));
                    empty = 0;
                }
                fen.append(Pieces::str(piece));
            }
            else
            {
                empty++;
            }
        }
        if (empty > 0) fen.append(std::to_string(empty));
        if (rank > RANK_1) fen.append("/");
    }
    
    if (m_side == WHITE) fen.append(" w ");
    else fen.append(" b ");
    
    if (canOO<WHITE>()) fen.append("K");
    if (canOOO<WHITE>()) fen.append("Q");
    if (canOO<BLACK>()) fen.append("k");
    if (canOOO<BLACK>()) fen.append("q");
    
    if (m_enpassant == SQUARE_NONE) fen.append(" - ");
    else fen.append(" " + Squares::str(m_enpassant) + " ");
    
    fen.append(std::to_string(m_fifty) + " ");
    fen.append(std::to_string(m_fullmove));
    
    return fen;
}

std::string Board::ascii()
{
    static const std::string pieces = ".PNBRQKpnbrqk";
    std::string str = "\n";
    Square start, end;
    
    str.append("Fen : ");
    str.append(fen());
    str.append("\n");
    str.append("Side : ");
    str.append(m_side == WHITE ? "white\n" : "black\n");
    
    str.append("Castling : ");
    if (m_castling & WHITE_OO)  str.append("K");
    if (m_castling & WHITE_OOO) str.append("Q");
    if (m_castling & BLACK_OO)  str.append("k");
    if (m_castling & BLACK_OOO) str.append("q");
    str.append("\n");
    
    str.append("Fifty : ");
    str.append(std::to_string(m_fifty));
    str.append("\n");
    
    str.append("En-passant : ");
    str.append(m_enpassant != SQUARE_NONE ? Squares::str(m_enpassant) : "none");
    str.append("\n");
    
    str.append("Last move : ");
    if (!m_history.empty()) str.append(Moves::uci(m_history.back()->move));
    else str.append("none");
    str.append("\n");
    
    str.append("Evaluation : ");
    str.append(std::to_string(Eval::evaluate(this)));
    str.append("\n");

    str.append("Hashkey : ");
    str.append(std::to_string(m_hashkey));
    str.append("\n");

    str.append("EvalHash : ");
    str.append(std::to_string(eval_hashes[m_hashkey]));
    str.append("\n");

    for (Rank r = RANK_8; r >= RANK_1; --r)
    {
        str.append(std::to_string(r+1));
        str.append(" ");
        
        start = Square(r*8);
        end = Square(start+7);

        for (Square sq = start; sq <= end; ++sq)
        {
            str.append(" ");
            str.append(1, pieces[m_square_bb[sq]]);
            str.append(" ");
        }
        str.append("\n");
    }
    str.append("\n   A  B  C  D  E  F  G  H\n\n");
    
    return str;
}

void Board::init()
{
    Piece squares[64];
    
    for (Square sq = A1; sq <= H8; ++sq)
    {
        squares[sq] = EMPTY;
    }
    
    squares[A1] = W_ROOK;
    squares[B1] = W_KNIGHT;
    squares[C1] = W_BISHOP;
    squares[D1] = W_QUEEN;
    squares[E1] = W_KING;
    squares[F1] = W_BISHOP;
    squares[G1] = W_KNIGHT;
    squares[H1] = W_ROOK;
    
    squares[A8] = B_ROOK;
    squares[B8] = B_KNIGHT;
    squares[C8] = B_BISHOP;
    squares[D8] = B_QUEEN;
    squares[E8] = B_KING;
    squares[F8] = B_BISHOP;
    squares[G8] = B_KNIGHT;
    squares[H8] = B_ROOK;
    
    for (Square sq = A2; sq <= H2; ++sq)
    {
        squares[sq] = W_PAWN;
    }
    
    for (Square sq = A7; sq <= H7; ++sq)
    {
        squares[sq] = B_PAWN;
    }
    
    init(squares, WHITE, ANY_CASTLING, SQUARE_NONE, 0, 1);
}

void Board::init(std::string fen)
{
    Piece squares[64];
    
    for (Square sq = A1; sq <= H8; ++sq)
    {
        squares[sq] = EMPTY;
    }
    
    std::vector<std::string> fen_parts = split(fen, ' ');
    
    // Parse pieces positions
    std::vector<std::string> fen_ranks = split(fen_parts.at(0), '/');
    
    Square sq;
    for (Rank r = RANK_8; r >= RANK_1; --r)
    {
        sq = Squares::make(FILE_A, r);
        
        for (std::string::iterator it = fen_ranks.at(7-r).begin(); it != fen_ranks.at(7-r).end(); ++it)
        {
            switch (*it)
            {
                case 'P':
                    squares[sq++] = W_PAWN;
                    break;
                    
                case 'N':
                    squares[sq++] = W_KNIGHT;
                    break;
                    
                case 'B':
                    squares[sq++] = W_BISHOP;
                    break;
                    
                case 'R':
                    squares[sq++] = W_ROOK;
                    break;
                    
                case 'Q':
                    squares[sq++] = W_QUEEN;
                    break;
                    
                case 'K':
                    squares[sq++] = W_KING;
                    break;
                    
                case 'p':
                    squares[sq++] = B_PAWN;
                    break;
                    
                case 'n':
                    squares[sq++] = B_KNIGHT;
                    break;
                    
                case 'b':
                    squares[sq++] = B_BISHOP;
                    break;
                    
                case 'r':
                    squares[sq++] = B_ROOK;
                    break;
                    
                case 'q':
                    squares[sq++] = B_QUEEN;
                    break;
                    
                case 'k':
                    squares[sq++] = B_KING;
                    break;
                    
                default:
                    int nb_empty = (*it) - '0';
                    if (nb_empty > 0 && nb_empty <= 8)
                    {
                        while (nb_empty)
                        {
                            squares[sq++] = EMPTY;
                            nb_empty--;
                        }
                    }
                    break;
            }
        }
    }
    
    Color side;
    try
    {
        side = fen_parts.at(1) == "w" ? WHITE : BLACK;
    }
    catch (...)
    {
        side = WHITE;
    }
    
    std::string fen_castling;
    try
    {
        fen_castling = fen_parts.at(2);
    }
    catch (...)
    {
        fen_castling = "";
    }
    
    int castling = 0;
    for (std::string::const_iterator it=fen_castling.begin(); it != fen_castling.end(); ++it)
    {
        switch (*it)
        {
            case 'K':
                castling ^= WHITE_OO;
                break;
                
            case 'Q':
                castling ^= WHITE_OOO;
                break;
                
            case 'k':
                castling ^= BLACK_OO;
                break;
            
            case 'q':
                castling ^= BLACK_OOO;
                break;
        }
    }
    
    std::string fen_enpassant;
    try
    {
        fen_enpassant = fen_parts.at(3);
    }
    catch (...)
    {
        fen_enpassant = "-";
    }
    
    Square enpassant = SQUARE_NONE;
    if (fen_enpassant != "-")
    {
        enpassant = Squares::make(fen_enpassant);
    }
    
    int fifty;
    try
    {
        fifty = atoi(fen_parts.at(4).c_str());
    }
    catch (...)
    {
        fifty = 0;
    }
    
    int fullmove;
    try
    {
        fullmove = atoi(fen_parts.at(5).c_str());
    }
    catch (...)
    {
        fullmove = 0;
    }
    
    init(squares, side, castling, enpassant, fifty, fullmove);
}

void Board::init(Piece squares[], Color side, int castling, Square enpassant, int fifty, int fullmove)
{
    // Set all bitboards to 0
    for (Square sq = A1; sq <= H8; ++sq)
    {
        m_square_bb[sq] = EMPTY;
    }
    for (Piece p = W_PAWN; p <= B_KING; ++p)
    {
        m_piece_bb[p] = 0;
    }
    
    m_black_bb = 0;
    m_white_bb = 0;
    m_occupied_bb = 0;
    m_empty_bb = 0;
    m_hashkey = 0;

    //populate bitboards
    for (Square sq = A1; sq <= H8; ++sq)
    {
        Piece p = squares[sq];
        
        m_square_bb[sq] = p;
        m_piece_bb[p] |= BITSET[sq];
        
        if (p != EMPTY) m_hashkey ^= HASH_KEYS.keys[sq][p];
    }

    for (Piece p = W_PAWN; p <= W_KING; ++p)
    {
        m_white_bb |= m_piece_bb[p];
    }
    
    for (Piece p = B_PAWN; p <= B_KING; ++p)
    {
        m_black_bb |= m_piece_bb[p];
    }
    
    m_occupied_bb = m_white_bb | m_black_bb;
    
    // state of the board
    m_side = side;
    m_castling = castling;
    m_enpassant = enpassant;
    m_fifty = fifty;
    m_fullmove = fullmove;
    
    if (castling & WHITE_OOO) m_hashkey ^= HASH_KEYS.w_ooo;
    if (castling & WHITE_OO) m_hashkey ^= HASH_KEYS.w_oo;
    if (castling & BLACK_OOO) m_hashkey ^= HASH_KEYS.b_ooo;
    if (castling & BLACK_OO) m_hashkey ^= HASH_KEYS.b_oo;

    if (side) m_hashkey ^= HASH_KEYS.side;
    if (enpassant != SQUARE_NONE) m_hashkey ^= HASH_KEYS.enpassant[enpassant];
}

void Board::make_move(const Move m)
{
    m_history.push_back(new GameHistory(m, m_castling, m_enpassant, m_fifty, m_hashkey));
    
    Square sqFrom = Moves::from(m);
    Square sqTo = Moves::to(m);
    
    Piece piece = Moves::piece(m);
    Piece captured = Moves::capture(m);
    
    // "standard" move
    m_piece_bb[piece] ^= BITSET[sqFrom];
    m_piece_bb[piece] |= BITSET[sqTo];
    
    m_piece_bb[EMPTY] ^= BITSET[sqTo];
    m_piece_bb[EMPTY] |= BITSET[sqFrom];
    
    m_square_bb[sqFrom] = EMPTY;
    m_square_bb[sqTo] = piece;
    
    m_hashkey ^= (HASH_KEYS.keys[sqFrom][piece] ^ HASH_KEYS.keys[sqTo][piece]);
    if (m_enpassant != SQUARE_NONE) m_hashkey ^= HASH_KEYS.enpassant[m_enpassant];
    
    // set en-passant square
    if (piece == W_PAWN && Squares::rank(sqFrom) == RANK_2 && Squares::rank(sqTo) == RANK_4)
    {
        m_enpassant = static_cast<Square>(sqFrom + 8);
        m_hashkey ^= HASH_KEYS.enpassant[sqFrom + 8];
    }
    else if (piece == B_PAWN && Squares::rank(sqFrom) == RANK_7 && Squares::rank(sqTo) == RANK_5)
    {
        m_enpassant = static_cast<Square>(sqTo + 8);
        m_hashkey ^= HASH_KEYS.enpassant[sqTo + 8];
    }
    else
    {
        m_enpassant = SQUARE_NONE;
    }
    
    // update fifty-move rule
    if (piece == W_PAWN || piece == B_PAWN)
    {
        m_fifty = 0;
    }
    else
    {
        ++m_fifty;
    }

    if (Moves::is_enpassant(m))
    {
        if (piece == W_PAWN)
        {
            m_piece_bb[captured] ^= BITSET[sqTo-8];
            m_square_bb[sqTo-8] = EMPTY;
            m_hashkey ^= HASH_KEYS.keys[sqTo-8][B_PAWN];
        }
        else
        {
            m_piece_bb[captured] ^= BITSET[sqTo+8];
            m_square_bb[sqTo+8] = EMPTY;
            m_hashkey ^= HASH_KEYS.keys[sqTo+8][W_PAWN];
        }
    }
    else if (captured)
    {
        m_fifty = 0;
        m_piece_bb[captured] ^= BITSET[sqTo];
        m_hashkey ^= HASH_KEYS.keys[sqTo][captured];

        if (captured == W_ROOK)
        {
            if (sqTo == A1)
            {
                if (m_castling & WHITE_OOO) m_hashkey ^= HASH_KEYS.w_ooo;
                m_castling &= ~WHITE_OOO;
            }
            else if (sqTo == H1)
            {
                if (m_castling & WHITE_OO) m_hashkey ^= HASH_KEYS.w_oo;
                m_castling &= ~WHITE_OO;
            }
        }
        else if (captured == B_ROOK)
        {
            if (sqTo == A8)
            {
                if (m_castling & BLACK_OOO) m_hashkey ^= HASH_KEYS.b_ooo;
                m_castling &= ~BLACK_OOO;
            }
            else if (sqTo == H8)
            {
                if (m_castling & BLACK_OO) m_hashkey ^= HASH_KEYS.b_oo;
                m_castling &= ~BLACK_OO;
            }
        }
    }
    
    // promotion
    if (Moves::is_promotion(m))
    {
        Piece promotion = Moves::promotion(m);
        
        m_piece_bb[piece] ^= BITSET[sqTo];
        m_piece_bb[promotion] ^= BITSET[sqTo];
        m_square_bb[sqTo] = promotion;
        m_hashkey ^= HASH_KEYS.keys[sqTo][piece];
        m_hashkey ^= HASH_KEYS.keys[sqTo][promotion];
    }
    
    // castle
    if (Moves::is_castling(m))
    {
        if (piece == W_KING)
        {
            if (sqTo == G1)
            {
                m_piece_bb[W_ROOK] ^= BITSET[F1] | BITSET[H1];
                m_square_bb[H1] = EMPTY;
                m_square_bb[F1] = W_ROOK;
                m_hashkey ^= (HASH_KEYS.keys[H1][W_ROOK] ^ HASH_KEYS.keys[F1][W_ROOK]);
            }
            else if (sqTo == C1)
            {
                m_piece_bb[W_ROOK] ^= BITSET[A1] | BITSET[D1];
                m_square_bb[A1] = EMPTY;
                m_square_bb[D1] = W_ROOK;
                m_hashkey ^= (HASH_KEYS.keys[A1][W_ROOK] ^ HASH_KEYS.keys[D1][W_ROOK]);
            }
        }
        else if (piece == B_KING)
        {
            if (sqTo == G8)
            {
                m_piece_bb[B_ROOK] ^= BITSET[F8] | BITSET[H8];
                m_square_bb[H8] = EMPTY;
                m_square_bb[F8] = B_ROOK;
                m_hashkey ^= (HASH_KEYS.keys[H8][B_ROOK] ^ HASH_KEYS.keys[F8][B_ROOK]);
            }
            else if (sqTo == C8)
            {
                m_piece_bb[B_ROOK] ^= BITSET[A8] | BITSET[D8];
                m_square_bb[A8] = EMPTY;
                m_square_bb[D8] = B_ROOK;
                m_hashkey ^= (HASH_KEYS.keys[A8][B_ROOK] ^ HASH_KEYS.keys[D8][B_ROOK]);
            }
        }
    }
    
    if (piece == W_ROOK)
    {
        if (sqFrom == A1)
        {
            if (m_castling & WHITE_OOO) m_hashkey ^= HASH_KEYS.w_ooo;
            m_castling &= ~WHITE_OOO;
        }
        else if (sqFrom == H1)
        {
            if (m_castling & WHITE_OO) m_hashkey ^= HASH_KEYS.w_oo;
            m_castling &= ~WHITE_OO;
        }
    }
    else if (piece == W_KING && sqFrom == E1)
    {
        if (m_castling & WHITE_OOO) m_hashkey ^= HASH_KEYS.w_ooo;
        if (m_castling & WHITE_OO) m_hashkey ^= HASH_KEYS.w_oo;
        m_castling &= ~WHITE_OO;
        m_castling &= ~WHITE_OOO;
    }
    else if (piece == B_ROOK)
    {
        if (sqFrom == A8)
        {
            if (m_castling & BLACK_OOO) m_hashkey ^= HASH_KEYS.b_ooo;
            m_castling &= ~BLACK_OOO;
        }
        else if (sqFrom == H8)
        {
            if (m_castling & BLACK_OO) m_hashkey ^= HASH_KEYS.b_oo;
            m_castling &= ~BLACK_OO;
        }
    }
    else if (piece == B_KING && sqFrom == E8)
    {
        if (m_castling & BLACK_OOO) m_hashkey ^= HASH_KEYS.b_ooo;
        if (m_castling & BLACK_OO) m_hashkey ^= HASH_KEYS.b_oo;
        m_castling &= ~BLACK_OO;
        m_castling &= ~BLACK_OOO;
    }
        
    
    // update others bitboards
    m_white_bb = m_piece_bb[W_KING] | m_piece_bb[W_QUEEN] | m_piece_bb[W_BISHOP] | m_piece_bb[W_KNIGHT] | m_piece_bb[W_ROOK] | m_piece_bb[W_PAWN];
    m_black_bb = m_piece_bb[B_KING] | m_piece_bb[B_QUEEN] | m_piece_bb[B_BISHOP] | m_piece_bb[B_KNIGHT] | m_piece_bb[B_ROOK] | m_piece_bb[B_PAWN];
    m_occupied_bb = m_white_bb | m_black_bb;
    m_empty_bb = m_occupied_bb ^ 0xffffffffffffffffULL;

    if (m_side == BLACK) ++m_fullmove;    
    m_side = (m_side == WHITE ? BLACK : WHITE);
    m_hashkey ^= HASH_KEYS.side;

    assert(check_integrity(m_square_bb, m_piece_bb, m_empty_bb, m_occupied_bb, m_white_bb, m_black_bb));
}

void Board::unmake_move()
{
    const GameHistory* last_state = m_history.back();
    const Move m = last_state->move;

    Square sqFrom = Moves::from(m);
    Square sqTo = Moves::to(m);
    
    Bitboard fromToBB = BITSET[sqFrom] ^ BITSET[sqTo];
    
    Piece piece = Moves::piece(m);
    Piece captured = Moves::capture(m);
    
    m_piece_bb[piece] ^= fromToBB;
    m_piece_bb[EMPTY] ^= fromToBB;
    m_square_bb[sqFrom] = piece;
    m_square_bb[sqTo] = EMPTY;
    
    if (Moves::is_capture(m))
    {
        if (Moves::is_enpassant(m))
        {
            int epTo = sqTo + (piece == W_PAWN ? -8 : 8);
            m_piece_bb[captured] ^= BITSET[epTo];
            m_square_bb[epTo] = captured;
        }
        else
        {
            m_piece_bb[captured] ^= BITSET[sqTo];
            m_square_bb[sqTo] = captured;
        }
    }
    else if (Moves::is_castling(m))
    {
        if (sqTo == G1) 
        {
            m_piece_bb[W_ROOK] ^= BITSET[H1] ^ BITSET[F1];
            m_square_bb[H1] = W_ROOK;
            m_square_bb[F1] = EMPTY;
        }
        else if (sqTo == C1)
        {
            m_piece_bb[W_ROOK] ^= BITSET[A1] ^ BITSET[D1];
            m_square_bb[A1] = W_ROOK;
            m_square_bb[D1] = EMPTY;
        }
        else if (sqTo == G8)
        {
            m_piece_bb[B_ROOK] ^= BITSET[H8] ^ BITSET[F8];
            m_square_bb[H8] = B_ROOK;
            m_square_bb[F8] = EMPTY;
        }
        else if (sqTo == C8)
        {
            m_piece_bb[B_ROOK] ^= BITSET[A8] ^ BITSET[D8];
            m_square_bb[A8] = B_ROOK;
            m_square_bb[D8] = EMPTY;
        }
    }
    
    if (Moves::is_promotion(m))
    {
        m_piece_bb[Moves::promotion(m)] ^= BITSET[sqTo];
        m_piece_bb[piece] ^= BITSET[sqTo];
    }
    
    m_enpassant = last_state->enpassant;
    m_castling = last_state->castling;
    m_fifty = last_state->fifty;
    
    if (m_side == WHITE) --m_fullmove;
    
    // update others bitboards
    m_white_bb = m_piece_bb[W_KING] | m_piece_bb[W_QUEEN] | m_piece_bb[W_BISHOP] | m_piece_bb[W_KNIGHT] | m_piece_bb[W_ROOK] | m_piece_bb[W_PAWN];
    m_black_bb = m_piece_bb[B_KING] | m_piece_bb[B_QUEEN] | m_piece_bb[B_BISHOP] | m_piece_bb[B_KNIGHT] | m_piece_bb[B_ROOK] | m_piece_bb[B_PAWN];
    m_occupied_bb = m_white_bb | m_black_bb;
    m_empty_bb = m_occupied_bb ^ 0xffffffffffffffffULL;
    
    m_side = (m_side == WHITE ? BLACK : WHITE);
    m_hashkey = last_state->hashkey;

    delete last_state;
    m_history.pop_back();

    assert(check_integrity(m_square_bb, m_piece_bb, m_empty_bb, m_occupied_bb, m_white_bb, m_black_bb));
}

bool Board::check_integrity() const
{
    for (Square sq = A1; sq <= H8; ++sq)
    {
        Piece piece = static_cast<Piece>(m_square_bb[sq]);
        Color color = Pieces::color_of(piece);
        
        if ((m_piece_bb[piece] & BITSET[sq]) != BITSET[sq])
        {
            return false;
        }
        
        if (piece == EMPTY && (m_empty_bb & BITSET[sq]) != BITSET[sq])
        {
            return false;
        }
        
        if (piece != EMPTY)
        {
            if ((m_occupied_bb & BITSET[sq]) != BITSET[sq])
            {
                return false;
            }
        
            if (color == WHITE && (m_white_bb & BITSET[sq]) != BITSET[sq])
            {
                return false;
            }
            
            if (color == BLACK && (m_black_bb & BITSET[sq]) != BITSET[sq])
            {
                return false;
            }
        }
    }

    uint64_t key = 0;
    for (Square sq = A1; sq <= H8; ++sq)
    {
        if (m_square_bb[sq] != EMPTY) key ^= HASH_KEYS.keys[sq][m_square_bb[sq]];
    }
    if (m_castling & WHITE_OOO) key ^= HASH_KEYS.w_ooo;
    if (m_castling & WHITE_OO) key ^= HASH_KEYS.w_oo;
    if (m_castling & BLACK_OOO) key ^= HASH_KEYS.b_ooo;
    if (m_castling & BLACK_OO) key ^= HASH_KEYS.b_oo;
    if (m_side) key ^= HASH_KEYS.side;
    if (m_enpassant != SQUARE_NONE) key ^= HASH_KEYS.enpassant[m_enpassant];
    if (m_hashkey != key)
    {
        std::cout << "Bad hashkey" << std::endl;
        return false;
    }

    return true;
}

bool Board::check_integrity(Piece square_bb[], Bitboard piece_bb[], Bitboard empty_bb, Bitboard occupied_bb, Bitboard white_bb, Bitboard black_bb) const
{
    if (!check_integrity()) return false;
    
    for (Square sq = A1; sq <= H8; ++sq)
    {
        if (m_square_bb[sq] != square_bb[sq]) 
        {
            std::cout << "ko: square_bb[" << Squares::str(sq) << "]   [(actual)" << Pieces::str(static_cast<Piece>(m_square_bb[sq])) << "] != [(expected)" << Pieces::str(static_cast<Piece>(square_bb[sq])) << "]" << std::endl;
            return false;
        }
    }
 
    for (Piece p = EMPTY; p <= B_KING; ++p)
    {
        if (m_piece_bb[p] != piece_bb[p]) 
        {
            std::cout << "ko: piece_bb " << Pieces::str(p) << std::endl;
            std::cout << "expected : " << std::endl;
            std::cout << Bitboards::ascii(piece_bb[p]);
            std::cout << "actual : " << std::endl;
            std::cout << Bitboards::ascii(m_piece_bb[p]);
            return false;
        }
    }
    
    if (m_empty_bb != empty_bb)
    {
        std::cout << "ko: empty_bb" << std::endl;
        return false;
    }
    if (m_occupied_bb != occupied_bb) 
    {
        std::cout << "ko: occupied_bb" << std::endl;
        return false;
    }
    if (m_white_bb != white_bb) 
    {
        std::cout << "ko: white_bb" << std::endl;
        return false;
    }
    if (m_black_bb != black_bb) 
    {
        std::cout << "ko: black_bb" << std::endl;
        return false;
    }
    
    return true;
}

bool Board::position_is_legal()
{
    if (m_side == WHITE && MoveGen::isAttackedBy(m_piece_bb[B_KING], this, WHITE))
    {
        return false;
    }
    else if (m_side == BLACK && MoveGen::isAttackedBy(m_piece_bb[W_KING], this, BLACK))
    {
        return false;
    }
    
    return true;
}

bool Board::king_is_attacked()
{
    if (m_side == WHITE && MoveGen::isAttackedBy(m_piece_bb[W_KING], this, BLACK))
    {
        return true;
    }
    else if (m_side == BLACK && MoveGen::isAttackedBy(m_piece_bb[B_KING], this, WHITE))
    {
        return true;
    }
    
    return false;
}

unsigned long long Board::perft(int depth)
{
    if (depth == 0) return 1;
    
    unsigned long long nodes = 0;
    MoveList ml;
    MoveGen::generate_moves(ml, this);
    for (Move move : ml)
    {
#ifdef DEBUG_PERFT
        Bitboard sqbb[64];
        for (Square sq = A1; sq <= H8; ++sq)
        {
            sqbb[sq] = m_square_bb[sq];
        }
        
        Bitboard pibb[16];
        for (Piece p = EMPTY; p <= B_KING; ++p)
        {
            pibb[p] = m_piece_bb[p];
        }
        
        Bitboard embb = m_empty_bb;
        Bitboard ocbb = m_occupied_bb;
        Bitboard whbb = m_white_bb;
        Bitboard blbb = m_black_bb;
        
        std::string fen = this->fen();
#endif
        make_move(move);
#ifdef DEBUG_PERFT
        if (!check_integrity()) 
        {
            std::cout << std::endl;
            std::cout << "CHECK INTEGRITY FAILED AFTER MAKE MOVE " << move->uci_notation() << std::endl;
            std::cout << "HISTORY" << std::endl;
            for (GameHistory* gh : m_history) {
                std::cout << gh->move->uci_notation() << std::endl;
            }
            std::cout << std::endl;
            exit(1);
        }
#endif
        if (position_is_legal()) 
        {
            nodes += perft(depth - 1);
        }
        unmake_move();
#ifdef DEBUG_PERFT
        if (!check_integrity(sqbb, pibb, embb, ocbb, whbb, blbb) || fen != this->fen()) 
        {
            std::cout << std::endl;
            std::cout << "CHECK INTEGRITY FAILED AFTER UNMAKE MOVE " << move->uci_notation() << (move->is_promotion() ? "=" + Pieces::str(move->promotion()) : "") << std::endl;
            std::cout << "ACTUAL FEN   " << this->fen() << std::endl;
            std::cout << "EXPECTED FEN " << fen << std::endl;
            std::cout << "HISTORY" << std::endl;
            for (GameHistory* gh : m_history) {
                std::cout << gh->move->uci_notation() << std::endl;
            }
            std::cout << std::endl;
            std::cout << ascii() << std::endl;
            exit(1);
        }
#endif
    }
    
    return nodes;
}


void Board::mirror()
{
    std::stringstream mirror_fen;
    std::string curr_fen = fen();
    std::string pieces = curr_fen.substr(0, curr_fen.find(' '));
    
    std::vector<std::string> v_pieces = split(pieces, '/');
    for (std::vector<std::string>::const_reverse_iterator it = v_pieces.rbegin(); it != v_pieces.rend(); ++it)
    {
        const std::string rank_pieces = *it;
        for (const char& p : rank_pieces)
        {
            PieceType pt = Pieces::make(p);
            if (pt == NO_PIECE)
            {
                mirror_fen << p;
            }
            else if (std::isupper(p))
            {
                mirror_fen << Pieces::str(Pieces::make(p, BLACK));
            }
            else
            {
                mirror_fen << Pieces::str(Pieces::make(p, WHITE));
            }
        }
        
        if (it != (v_pieces.rend()-1)) mirror_fen << '/';
    }
    
    mirror_fen << " " << (m_side == WHITE ? "b" : "w");
    init(mirror_fen.str());
}

Bitboard Board::attackers_to(Square to, Bitboard occupied) const
{
    return   (PAWN_ATT[BLACK][to] & (m_piece_bb[W_PAWN]   | m_piece_bb[B_PAWN]))
           | (KNIGHT_ATT[to]      & (m_piece_bb[W_KNIGHT] | m_piece_bb[B_KNIGHT]))
           | ((MoveGen::noea_attack(occupied, to) | MoveGen::nowe_attack(occupied, to) | MoveGen::soea_attack(occupied, to) | MoveGen::sowe_attack(occupied, to)) & (m_piece_bb[W_BISHOP] | m_piece_bb[B_BISHOP] | m_piece_bb[W_QUEEN] | m_piece_bb[B_QUEEN]))
           | ((MoveGen::file_attack(occupied, to) | MoveGen::rank_attack(occupied, to)) & (m_piece_bb[W_ROOK] | m_piece_bb[B_ROOK] | m_piece_bb[W_QUEEN] | m_piece_bb[B_QUEEN]))
           | (KING_ATT[to]        & (m_piece_bb[W_KING]   | m_piece_bb[B_KING]));
}

PieceType Board::next_attacker(Square to, Bitboard& attackers, Bitboard& stm_attackers, Bitboard& occupied, PieceType pt)
{
    if (pt == KING) return KING;

    Bitboard pt_stm_att = (m_piece_bb[Pieces::make(pt, WHITE)] | m_piece_bb[Pieces::make(pt, BLACK)]) & stm_attackers;
    if (!pt_stm_att)
    {
        return next_attacker(to, attackers, stm_attackers, occupied, (static_cast<PieceType>(pt+1)));
    }

    occupied ^= pt_stm_att & ~(pt_stm_att - 1);

    if (pt == PAWN || pt == BISHOP || pt == QUEEN)
    {
        attackers |= ((MoveGen::noea_attack(occupied, to) | MoveGen::nowe_attack(occupied, to) | MoveGen::soea_attack(occupied, to) | MoveGen::sowe_attack(occupied, to)) & (m_piece_bb[W_BISHOP] | m_piece_bb[B_BISHOP] | m_piece_bb[W_QUEEN] | m_piece_bb[B_QUEEN]));
    }

    if (pt == ROOK || pt == QUEEN)
    {
        attackers |= ((MoveGen::file_attack(occupied, to) | MoveGen::rank_attack(occupied, to)) & (m_piece_bb[W_ROOK] | m_piece_bb[B_ROOK] | m_piece_bb[W_QUEEN] | m_piece_bb[B_QUEEN]));
    }

    attackers &= occupied;
    return pt;
}

int Board::repetition_count()
{
    if (m_history.size() <= 3) return 1;

    int counter = 1;
    int start = m_history.size()-2;
    int last = m_history.size()-m_fifty;
    for (int i = start; i >= last; i -= 2)
    {
        if (m_history.at(i)->hashkey == m_hashkey) ++counter;
    }
    
    return counter;
}

bool Board::is_endgame() const
{
    Bitboard queens = m_piece_bb[W_QUEEN] | m_piece_bb[B_QUEEN];
    if (!queens) return true;

    // one queen + one minor max
    Bitboard minors = m_piece_bb[W_ROOK] | m_piece_bb[W_BISHOP] | m_piece_bb[W_KNIGHT];
    if (m_piece_bb[W_QUEEN] && Bitboards::pop_count(minors) <= 1) return true;

    minors = m_piece_bb[B_ROOK] | m_piece_bb[B_BISHOP] | m_piece_bb[B_KNIGHT];
    if (m_piece_bb[B_QUEEN] && Bitboards::pop_count(minors) <= 1) return true;

    return false;
}


