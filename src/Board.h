#ifndef BOARD_H
#define BOARD_H

#include "bitboard.h"
#include "castling.h"
#include "GameHistory.h"
#include "material.h"
#include "Hash.h"
#include "move.h"
#include "piece.h"
#include "utility.h"
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <cctype>
#include <stdint.h>

class Board
{
public:
    Board();
    ~Board();
    
    /**
     * @brief Initialize the board with stardard initial position.
     */
    void init();
    
    /**
     * @brief Initialize the board with fen.
     * @param fen
     */
    void init(std::string fen);
    
    /**
     * @brief Initialize the board
     * @param squares
     * @param side
     * @param castling
     * @param enpassant
     * @param fifty
     */
    void init( Piece squares[], Color side, int castling, Square enpassant, int fifty, int fullmove );
    
    const std::vector<GameHistory*>history() const
    {
        return m_history;
    }
    
    /**
     * @brief Returns bitboard with position of pieces P.
     * @return
     */
    Bitboard piece_bb(Piece p) const
    {
        return m_piece_bb[p];
    }
    
    Bitboard piece_bb(Color c, PieceType p) const
    {
        Piece piece = static_cast<Piece>(p + (c == BLACK ? 6 : 0));
        return m_piece_bb[piece];
    }
    
    /**
     * @brief Returns bitboard with empty squares.
     * @return
     */
    Bitboard empty_bb() const
    {
        return m_empty_bb;
    }
    
    /**
     * @brief Returns bitboard with occupied squares.
     * @return
     */
    Bitboard occupied_bb() const
    {
        return m_occupied_bb;
    }
    
    /**
     * @brief Returns WHITE bitboard
     * @return
     */
    Bitboard white_bb() const
    {
        return m_white_bb;
    }
    
    /**
     * @brief Returns BLACK bitboard
     * @return
     */
    Bitboard black_bb() const
    {
        return m_black_bb;
    }
    
    Bitboard color_bb(Color c) const
    {
        return c == WHITE ? m_white_bb : m_black_bb;
    }
    
    Piece square_bb(Square sq) const
    {
        return m_square_bb[sq];
    }
    
    /**
     * @brief Returns enpassant square.
     * @return
     */
    const Square& enpassant() const
    {
        return m_enpassant;
    }
    
    template <Color C>
    /**
     * @brief Return OO state.
     * @return
     */
    bool canOO() const
    {
        return m_castling & (C == WHITE ? WHITE_OO : BLACK_OO);
    }
    
    template <Color C>
    /**
     * @brief Return OOO state.
     * @return
     */
    bool canOOO() const
    {
        return m_castling & (C == WHITE ? WHITE_OOO : BLACK_OOO);
    }
    
    const Color& side() const
    {
        return m_side;
    }
    
    /**
     * @brief Returns a string with all bitboards. Useful for debugging.
     * @return 
     */
    std::string dump_all_bb();
    
    std::string fen() const;
    
    /**
     * @brief Returns an ascii representation of the board.
     * @return 
     */
    std::string ascii();
    
    /**
     * @brief Verify integrity of all bitboards.
     * @return
     */
    bool check_integrity() const;
    bool check_integrity( Piece square_bb[], Bitboard piece_bb[], Bitboard empty_bb, Bitboard occupied_bb, Bitboard white_bb, Bitboard black_bb ) const;
    
    /**
     * @brief Apply the move
     * @param m
     */
    void make_move( const Move m );
    
    /**
     * @brief Unmake the move
     */
    void unmake_move();
    bool position_is_legal();
    bool king_is_attacked();
    unsigned long long perft(int depth);
    void mirror();

    Bitboard attackers_to( Square to, Bitboard occupied ) const;
    PieceType next_attacker( Square to, Bitboard &attackers, Bitboard &stm_attackers, Bitboard &occupied, PieceType pt = PAWN );
    
    int repetition_count();

    int fifty() const
    {
        return m_fifty;
    };

    bool is_endgame() const;
    
    uint64_t hashkey() const { return m_hashkey; }

private:
    Piece  m_square_bb[64];
    Bitboard  m_piece_bb[13];
    
    Bitboard& m_empty_bb = m_piece_bb[EMPTY];
    Bitboard  m_occupied_bb;
    
    Bitboard  m_white_bb;
    Bitboard  m_black_bb;
    
    Color m_side;
    Square m_enpassant;
    int m_castling;
    int m_fifty;
    int m_fullmove;
    
    uint64_t m_hashkey; 
    
    std::vector<GameHistory*> m_history;
};

#endif // BOARD_H
