#include "BullitChess.h"

UciOptions ucioptions;

BullitChess::BullitChess()
{
    Bitboards::init();
    HASH_KEYS.init();
    m_board.init();
}

BullitChess::~BullitChess()
{
}

void BullitChess::run()
{
    std::setbuf(stdout, NULL);
    
    std::thread console_thread(std::ref(Console::get_instance()));
    std::string cmd_name, parameters;
    
    console_thread.detach();

    std::cout << std::fixed;
    
#ifdef DEBUG_IO
    std::ofstream iologfile("io.log");
#endif
    
    while (true)
    {
        std::stringstream input;
        input << Console::get_instance().get_input();
        
        if (input.rdbuf()->in_avail())
        {
#ifdef DEBUG_IO
            iologfile << "< " << input.str() << std::endl;
#endif
            
            std::getline(input, cmd_name, ' ');
            std::getline(input, parameters);
            
            if (cmd_name == "quit" || cmd_name == "exit")
            {
                break;
            }
            else if (cmd_name == "help" || cmd_name == "?" || cmd_name == "h")
            {
                std::printf("help:\n");
                std::printf("\n");
                std::printf("bb        : Print all bitboards.\n");
                std::printf("check     : Check integrity of the board.\n");
                std::printf("display   : Show board representation.\n");
                std::printf("move      : Make a move.\n");
                std::printf("position  : Initialize a position.\n");
                std::printf("\n");
            }
            else if (cmd_name == "bb")
            {
                std::printf("%s", m_board.dump_all_bb().c_str());
            }
            else if (cmd_name == "display")
            {
                std::printf("%s", m_board.ascii().c_str());
            }
            else if (cmd_name == "position")
            {
                try
                {
                    if (parameters.find("startpos") != std::string::npos)
                    {
                        m_board.init();
                        std::size_t moves_pos = parameters.find("moves");
                        if (moves_pos != std::string::npos)
                        {
                            std::string moves = parameters.substr(moves_pos+6);
                            std::size_t move_pos = 0;
                            while (moves.size() && move_pos != std::string::npos)
                            {
                                move_pos = moves.find(" ");
                                
                                std::string movestr = moves.substr(0, move_pos);
                                if (move_pos != std::string::npos)
                                {
                                    moves.erase(0, move_pos+1);
                                }

                                Square from = Squares::make(movestr.substr(0, 2));
                                Square to   = Squares::make(movestr.substr(2, 4));
                                Piece piece = static_cast<Piece>(m_board.square_bb(from));
                                Piece capture = static_cast<Piece>(m_board.square_bb(to));
                                
                                Move move;
                                if (movestr.size() == 5)
                                {
                                    Piece promotion = Pieces::make(movestr.at(4), m_board.side());
                                    move = Moves::create(from, to, piece, capture, promotion);
                                }
                                else if ( (piece == W_KING && from == E1 && (to == G1 || to == C1)) || (piece == B_KING && from == E8 && (to == G8 || to == C8)) )
                                {
                                    move = Moves::create(from, to, piece, CASTLING);
                                }
                                else if (to == m_board.enpassant() && (piece == W_PAWN || piece == B_PAWN))
                                {
                                    move = Moves::create(from, to, piece, Pieces::make(PAWN, !m_board.side()), ENPASSANT);
                                }
                                else
                                {
                                    move = Moves::create(from, to, piece, capture);
                                }
                                
                                m_board.make_move(move);
                            }
                        }
                    }
                    else
                    {
                        m_board.init(parameters);
                    }
                }
                catch (...) {}
            }
            else if (cmd_name == "check")
            {
                if (m_board.check_integrity())
                {
                    std::printf("%s", "check OK\n");
                }
                else
                {
                    std::printf("%s", "check KO\n");
                }
            }
            else if (cmd_name == "move")
            {
                try
                {
                    Square from = Squares::make(parameters.substr(0, 2));
                    Square to = Squares::make(parameters.substr(2, 2));
                    
                    Piece promotion = EMPTY;
                    char c = 'Q';
                    try
                    {
                        c = parameters.substr(5, 1)[0];
                    }
                    catch (...) {}
                    
                    switch (c)
                    {
                        case 'R':
                            promotion = W_ROOK;
                            break;
                        case 'r':
                            promotion = B_ROOK;
                            break;
                            
                        case 'N':
                            promotion = W_KNIGHT;
                            break;
                        case 'n':
                            promotion = B_KNIGHT;
                            break;
                            
                        case 'B':
                            promotion = W_BISHOP;
                            break;
                        case 'b':
                            promotion = B_BISHOP;
                            break;
                            
                        case 'Q':
                            promotion = W_QUEEN;
                            break;
                        case 'q':
                            promotion = B_QUEEN;
                            break;
                            
                        default:
                            promotion = m_board.side() == WHITE ? W_QUEEN : B_QUEEN;
                    }
                    
                    MoveList ml;
                    MoveGen::generate_moves(ml, &m_board);
                    
                    bool moveIsValid = false;
                    for (const Move m : ml)
                    {
                        if ((Moves::from(m) == from && Moves::to(m) == to) && ((Moves::is_promotion(m) && Moves::promotion(m) == promotion) || !Moves::is_promotion(m)))
                        {
                            moveIsValid = true;
                            m_board.make_move(m);
                            break;
                        }
                    }
                    
                    if (!moveIsValid)
                    {
                        std::printf("bad move\n");
                    }
                }
                catch (...) {}
            }
            else if (cmd_name == "undo")
            {
                m_board.unmake_move();
            }
            else if (cmd_name == "list")
            {
                MoveList ml;
                MoveGen::generate_moves(ml, &m_board);
                
                std::cout << "Found " << ml.size() << " moves" << std::endl;
                for (const Move m : ml)
                {
                    m_board.make_move(m);
                    std::cout << (m_board.position_is_legal() ? "L " : "  ") << Moves::uci(m) << std::endl;
                    m_board.unmake_move();
                }
            } 
            else if (cmd_name == "history")
            {
                for (GameHistory* h : m_board.history())
                {
                    std::cout << Moves::uci(h->move) << std::endl;
                }
            }
            else if (cmd_name == "perft")
            {
                Timer timer = Timer();
                unsigned long long nodes = m_board.perft(std::stoi(parameters));
                std::cout << "Time elapsed: " << timer.ms() << "ms" << std::endl;
                std::cout << "Nodes count : " << nodes << std::endl;
            }
            else if (cmd_name == "mirror")
            {
                m_board.mirror();
            }
            else if (cmd_name == "p1")
            {
                m_board.init("k7/8/5n2/3p4/8/2N2B2/8/K7 w - - 0 1");
                std::cout << m_board.ascii() << std::endl;
            }
            else if (cmd_name == "p2")
            {
                m_board.init("2K5/8/8/3pRrRr/8/8/8/2k5 w - - 0 1");
                std::cout << m_board.ascii() << std::endl;
            }
            else if (cmd_name == "p3")
            {
                m_board.init("1r3rk1/1pp2qpp/p1np4/8/2Q5/2P1B3/P1P2PPP/R4RK1 w - - 1 21");
                std::cout << m_board.ascii() << std::endl;
            }
            else if (cmd_name == "p4")
            {
            }
            else if (cmd_name == "p5")
            {
                m_board.init("k7/8/4B3/N7/8/1r5R/Q1P5/7K w - - 0 1");
                std::cout << m_board.ascii() << std::endl;
            }
            else if (cmd_name == "d")
            {
                /*Square from = Squares::make(parameters.substr(0, 2));
                Square to = Squares::make(parameters.substr(2, 2));
                Piece piece = static_cast<Piece>(m_board.square_bb(from));
                Piece capture = static_cast<Piece>(m_board.square_bb(to));
                Move move = Moves::create(from, to, piece, capture);

                std::cout << "SEE=" << Eval::see(&m_board, move) << std::endl;*/
                
                std::cout << m_board.repetition_count() << std::endl;
            }
            else if (cmd_name == "uci")
            {
                ucioptions = UciOptions();
                std::cout << "id name BullitChess " << ENGINE_VERSION  << std::endl;
                std::cout << "id author Arnaud Halle" << std::endl;
                std::cout << "option name Hash type spin default " << ucioptions.hash << " min 1 max " << (32*1024*1024) << std::endl;
                std::cout << "uciok" << std::endl;
            }
            else if (cmd_name == "isready")
            {
                std::cout << "readyok" << std::endl;
            }
            else if (cmd_name == "setoption")
            {
                size_t pos;
                size_t space_pos;

                if (parameters.find("Hash") != std::string::npos)
                {
                    pos = parameters.find("value");
                    if (pos != std::string::npos)
                    {
                        space_pos = parameters.find(' ', pos+6);
                        ucioptions.hash = std::stoi(parameters.substr(pos+6, space_pos));
                    }
                }
            }
            else if (cmd_name == "go") 
            {
                int depth = 0;
                int maxtime = 0;
                int increment = 0;
                int movestogo = 0;

                size_t pos;
                size_t space_pos;
                
                pos = parameters.find("depth");
                if (pos != std::string::npos)
                {
                    space_pos = parameters.find(" ", pos+6);
                    depth = std::stoi(parameters.substr(pos+6, space_pos));
                }
                
                
                if (m_board.side() == WHITE)
                {
                    pos = parameters.find("wtime");
                }
                else
                {
                    pos = parameters.find("btime");
                }
                if (pos != std::string::npos)
                {
                    space_pos = parameters.find(' ', pos+6);
                    maxtime = std::stoi(parameters.substr(pos+6, space_pos));
                }
                
                if (m_board.side() == WHITE)
                {
                    pos = parameters.find("winc");
                }
                else
                {
                    pos = parameters.find("binc");
                }
                if (pos != std::string::npos)
                {
                    space_pos = parameters.find(" ", pos+6);
                    increment = std::stoi(parameters.substr(pos+5, space_pos));
                }
                
                pos = parameters.find("movestogo");
                if (pos != std::string::npos)
                {
                    space_pos = parameters.find(" ", pos+6);
                    movestogo = std::stoi(parameters.substr(pos+10, space_pos));
                }
                
                Search search(&m_board, depth, movestogo, increment, maxtime);
                Move bestmove = search.think();
                if (bestmove != Moves::MOVE_NONE)
                {
                    std::cout << "bestmove " << Moves::uci(bestmove) << std::endl;
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    
#ifdef DEBUG_IO
    iologfile.close();
#endif
}