#ifndef BULLITCHESS_H
#define BULLITCHESS_H

#include "movegen.h"
#include "bitboard.h"
#include "Board.h"
#include "search.h"
#include "Console.h"
#include "Timer.h"
#include "Hash.h"
#include "ucioptions.h"
#include <cstdio>
#include <fstream>
#include <sstream>
#include <string>
#include <thread>
#include <ios>

#define ENGINE_VERSION "1.1.0"

class BullitChess
{
public:
    BullitChess();
    ~BullitChess();
    
    /**
     * @brief Start engine.
     */
    void run();
private:
    Board m_board;
};

extern UciOptions ucioptions;

#endif // BULLITCHESS_H
