#include "Console.h"
#include <iostream>

Console &Console::get_instance()
{
    static Console con;
    return con;
}

void Console::operator()()
{
    std::string input;
    while (true)
    {
        std::getline(std::cin, input);
        
        std::lock_guard<std::mutex> lock(m_lock);
        m_inputs.push(input);
    }
}

std::string Console::get_input()
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_inputs.empty())
    {
        return "";
    }
    
    std::string input = m_inputs.front();
    m_inputs.pop();
    
    return input;
}
