#ifndef CONSOLE_H
#define CONSOLE_H

#include <mutex>
#include <queue>
#include <string>
#include <fstream>

class Console
{
public:
    static Console& get_instance();
    
    void operator()();
    std::string get_input();
    
private:
    Console() {};
    
    std::queue<std::string> m_inputs;
    std::mutex m_lock;
    
public:
    Console (Console const&) = delete;
    void operator=(Console const&) = delete;
};

#endif // CONSOLE_H
