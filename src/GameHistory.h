#ifndef GAMEHISTORY_H
#define GAMEHISTORY_H

#include "move.h"
#include "castling.h"
#include <stdint.h>

class GameHistory
{
public:
    const Move move;
    const int castling;
    const Square enpassant;
    const int fifty;
    const uint64_t hashkey;
    
    GameHistory(const Move move, int castling, Square enpassant, int fifty, uint64_t hashkey)
        : move(move), castling(castling), enpassant(enpassant), fifty(fifty), hashkey(hashkey) {}
};

#endif // GAMEHISTORY_H
