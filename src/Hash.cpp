#include "Hash.h"
#include <iostream>
Hash HASH_KEYS;

void Hash::init()
{
    time_t now;
    
    srand(std::time(&now));
    
    for (int i = 0; i < 64; ++i)
    {
        enpassant[i] = rand64();
        for (int j = 0; j < 16; ++j)
        {
            keys[i][j] = rand64();
        }
    }
    
    side = rand64();
    w_oo = rand64();
    w_ooo = rand64();
    b_oo = rand64();
    b_ooo = rand64();
}

uint64_t Hash::rand64()
{
    return rand() ^ (static_cast<uint64_t>(rand()) << 15) ^ (static_cast<uint64_t>(rand()) << 30) ^ (static_cast<uint64_t>(rand()) << 45) ^ (static_cast<uint64_t>(rand()) << 60);
}
