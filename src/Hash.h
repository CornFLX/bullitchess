#ifndef HASH_H
#define HASH_H

#include <stdlib.h>
#include <ctime>
#include <stdint.h>

struct Hash
{
    uint64_t keys[64][16];
    uint64_t side;
    uint64_t enpassant[64];
    uint64_t w_oo;
    uint64_t w_ooo;
    uint64_t b_oo;
    uint64_t b_ooo;
    
    void init();
    uint64_t rand64();
};

extern Hash HASH_KEYS;

#endif // HASH_H
