#include "Timer.h"

Timer::Timer()
{
    m_start = std::chrono::system_clock::now();
}

double Timer::ms()
{
    std::chrono::system_clock::time_point end = std::chrono::system_clock::now();
    std::chrono::duration<float, std::milli> elapsed = end - m_start;
    
    return elapsed.count();
}

void Timer::reset()
{
    m_start = std::chrono::system_clock::now();
}