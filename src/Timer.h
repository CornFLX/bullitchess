#ifndef TIMER_H
#define TIMER_H

#include <chrono>

class Timer
{
public:
    Timer();
    
    double ms();
    void reset();
private:
    std::chrono::system_clock::time_point m_start;
};

#endif // TIMER_H
