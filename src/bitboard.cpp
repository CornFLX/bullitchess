#include "bitboard.h"


Bitboard BITSET[64];

Bitboard PAWN_PUSH[2][64];
Bitboard PAWN_DBLPUSH[2][64];
Bitboard PAWN_ATT[2][64];

Bitboard KNIGHT_ATT[64];
Bitboard BISHOP_ATT[64];
Bitboard ROOK_ATT[64];
Bitboard QUEEN_ATT[64];
Bitboard KING_ATT[64];

Bitboard NORT_ATT[64];
Bitboard NOEA_ATT[64];
Bitboard EAST_ATT[64];
Bitboard SOEA_ATT[64];
Bitboard SOUT_ATT[64];
Bitboard SOWE_ATT[64];
Bitboard WEST_ATT[64];
Bitboard NOWE_ATT[64];


const std::string Bitboards::ascii(Bitboard b)
{
    std::string s = "";
    
    for (Rank rank = RANK_8; rank >= RANK_1; --rank)
    {
        for (File file = FILE_A; file <= FILE_H; ++file)
        {
            s.append((b & BITSET[Squares::make(file, rank)]) ? "1" : "0");
        }
        s.append("\n");
    }
    
    return s;
}

void Bitboards::init()
{
    Bitboard nort = 0x0101010101010100ULL;
    Bitboard sout = 0x0080808080808080ULL;
    
    Bitboard not_A_file  = 0xfefefefefefefefeULL;
    Bitboard not_AB_file = 0xfcfcfcfcfcfcfcfcULL;
    Bitboard not_H_file  = 0x7f7f7f7f7f7f7f7fULL;
    Bitboard not_GH_file = 0x3f3f3f3f3f3f3f3fULL;
    
    Rank rank;
    File file;
    
    int nb, pos_to_set;
    
    for (Square sq = A1; sq <= H8; ++sq)
    {
        rank = Squares::rank(sq);
        file = Squares::file(sq);
        
        BITSET[sq] |= 1ULL << sq;
        
        PAWN_PUSH[WHITE][sq] |= BITSET[sq] << 8;
        PAWN_PUSH[BLACK][sq] |= BITSET[sq] >> 8;
        
        if (sq >= A2 && sq <= H2)
        {
            PAWN_DBLPUSH[WHITE][sq] |= BITSET[sq] << 16;
        }
        if (sq >= A7 && sq <= H7)
        {
            PAWN_DBLPUSH[BLACK][sq] |= BITSET[sq] >> 16;
        }
        
        PAWN_ATT[WHITE][sq] |= (BITSET[sq] << 9 & not_A_file) | (BITSET[sq] << 7 & not_H_file);
        PAWN_ATT[BLACK][sq] |= (BITSET[sq] >> 9 & not_H_file) | (BITSET[sq] >> 7 & not_A_file);
        
        KNIGHT_ATT[sq] |= (BITSET[sq] << 17 & not_A_file);
        KNIGHT_ATT[sq] |= (BITSET[sq] << 10 & not_AB_file);
        KNIGHT_ATT[sq] |= (BITSET[sq] >> 6 & not_AB_file);
        KNIGHT_ATT[sq] |= (BITSET[sq] >> 15 & not_A_file);
        KNIGHT_ATT[sq] |= (BITSET[sq] >> 17 & not_H_file);
        KNIGHT_ATT[sq] |= (BITSET[sq] >> 10 & not_GH_file);
        KNIGHT_ATT[sq] |= (BITSET[sq] << 6 & not_GH_file);
        KNIGHT_ATT[sq] |= (BITSET[sq] << 15 & not_H_file);
                
        KING_ATT[sq] |= (BITSET[sq] << 8);
        KING_ATT[sq] |= (BITSET[sq] << 9 & not_A_file);
        KING_ATT[sq] |= (BITSET[sq] << 1 & not_A_file);
        KING_ATT[sq] |= (BITSET[sq] >> 7 & not_A_file);
        KING_ATT[sq] |= (BITSET[sq] >> 8);
        KING_ATT[sq] |= (BITSET[sq] >> 9 & not_H_file);
        KING_ATT[sq] |= (BITSET[sq] >> 1 & not_H_file);
        KING_ATT[sq] |= (BITSET[sq] << 7 & not_H_file);
        
        
        NORT_ATT[sq] |= nort << sq;
        SOUT_ATT[sq] |= sout >> (63-sq);
        EAST_ATT[sq] |= 2 * ((1ULL << (sq | 7)) - BITSET[sq]);
        WEST_ATT[sq] |= BITSET[sq] - (1ULL << (sq & 56));
        
        nb = ((8 - rank) < (8 - file)) ? (8 - rank) : (8 - file);
        pos_to_set = sq;
        for (int i = 0; i < nb; i++)
        {
            NOEA_ATT[sq] |= ((1ULL << pos_to_set) << 9ULL) & not_A_file;
            pos_to_set += 9;
        }
        
        nb = ((8 - rank) < file) ? (8 - rank) : file;
        pos_to_set = sq;
        for (int i = 0; i < nb; i++)
        {
            NOWE_ATT[sq] |= ((1ULL << pos_to_set) << 7ULL) & not_H_file;
            pos_to_set += 7;
        }
        
        
        nb = (rank < (8 - file)) ? rank : (8 - file);
        pos_to_set = sq;
        for (int i = 0; i < nb; i++)
        {
            SOEA_ATT[sq] |= ((1ULL << pos_to_set) >> 7ULL) & not_A_file;
            pos_to_set -= 7;
        }
        
        nb = (static_cast<int>(rank) < static_cast<int>(file)) ? static_cast<int>(rank) : static_cast<int>(file);
        pos_to_set = sq;
        for (int i = 0; i < nb; i++)
        {
            SOWE_ATT[sq] |= ((1ULL << pos_to_set) >> 9ULL) & not_H_file;
            pos_to_set -= 9;
        }
        
        ROOK_ATT[sq] |= EAST_ATT[sq] | WEST_ATT[sq] | NORT_ATT[sq] | SOUT_ATT[sq];
        BISHOP_ATT[sq] |= NOEA_ATT[sq] | SOEA_ATT[sq] | SOWE_ATT[sq] | NOWE_ATT[sq];
        QUEEN_ATT[sq] |= ROOK_ATT[sq] | BISHOP_ATT[sq];
    }
}

int Bitboards::bsf(Bitboard b)
{
    assert(b != 0);
    
    static const int debruijnIndex[64] = {
         0,  1, 48,  2, 57, 49, 28,  3,
        61, 58, 50, 42, 38, 29, 17,  4,
        62, 55, 59, 36, 53, 51, 43, 22,
        45, 39, 33, 30, 24, 18, 12,  5,
        63, 47, 56, 27, 60, 41, 37, 16,
        54, 35, 52, 21, 44, 32, 23, 11,
        46, 26, 40, 15, 34, 20, 31, 10,
        25, 14, 19,  9, 13,  8,  7,  6
    };
    
    return debruijnIndex[((b & -b) * 0x03f79d71b4cb0a89) >> 58];
}

int Bitboards::bsr(Bitboard b)
{
    assert(b != 0);
    
    static const int debruijnIndex[64] = {
        0, 47,  1, 56, 48, 27,  2, 60,
        57, 49, 41, 37, 28, 16,  3, 61,
        54, 58, 35, 52, 50, 42, 21, 44,
        38, 32, 29, 23, 17, 11,  4, 62,
        46, 55, 26, 59, 40, 36, 15, 53,
        34, 51, 20, 43, 31, 22, 10, 45,
        25, 39, 14, 33, 19, 30,  9, 24,
        13, 18,  8, 12,  7,  6,  5, 63
    };
    
    b |= b >> 1; 
    b |= b >> 2;
    b |= b >> 4;
    b |= b >> 8;
    b |= b >> 16;
    b |= b >> 32;
    
    return debruijnIndex[(b * 0x03f79d71b4cb0a89) >> 58];
}

int Bitboards::pop_count(Bitboard b)
{
    b = b - ((b >> 1) & 0x5555555555555555);
    b = (b & 0x3333333333333333) + ((b >> 2) & 0x3333333333333333);
    b = (b + (b >> 4)) & 0x0f0f0f0f0f0f0f0f;
    return (b * 0x0101010101010101) >> 56;
}