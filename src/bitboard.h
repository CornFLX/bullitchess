#ifndef BITBOARD_H
#define BITBOARD_H

#include "color.h"
#include "square.h"
#include <cassert>
#include <string>

typedef uint64_t Bitboard;

namespace Bitboards
{
    /**
     * @brief Initialize all bitboards utilities.
     */
    void init();
    
    /**
     * @brief Returns an ASCII representation of the bitboard.
     * @param b
     * @return 
     */
    const std::string ascii(Bitboard b);
    
    /**
     * @brief Returns the index of the least significant 1 bit (LS1B)
     * @param b
     * @return
     */
    int bsf(Bitboard b);
    
    /**
     * @brief Returns the index of the most significant 1 bit (MS1B)
     * @param b
     * @return
     */
    int bsr(Bitboard b);
    
    int pop_count(Bitboard b);
}

extern Bitboard BITSET[64];

extern Bitboard PAWN_PUSH[2][64];
extern Bitboard PAWN_DBLPUSH[2][64];
extern Bitboard PAWN_ATT[2][64];

extern Bitboard KNIGHT_ATT[64];
extern Bitboard BISHOP_ATT[64];
extern Bitboard ROOK_ATT[64];
extern Bitboard QUEEN_ATT[64];
extern Bitboard KING_ATT[64];

extern Bitboard NORT_ATT[64];
extern Bitboard NOEA_ATT[64];
extern Bitboard EAST_ATT[64];
extern Bitboard SOEA_ATT[64];
extern Bitboard SOUT_ATT[64];
extern Bitboard SOWE_ATT[64];
extern Bitboard WEST_ATT[64];
extern Bitboard NOWE_ATT[64];

#endif // BITBOARD_H