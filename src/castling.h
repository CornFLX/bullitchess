#ifndef CASTLING_H
#define CASTLING_H

enum CastlingRight
{
    NO_CASTLING,
    WHITE_OO, // = 1 << 0
    WHITE_OOO = 1 << 1,
    BLACK_OO  = 1 << 2,
    BLACK_OOO = 1 << 3,
    ANY_CASTLING = WHITE_OO | WHITE_OOO | BLACK_OO | BLACK_OOO
};

#endif // CASTLING_H