#ifndef COLOR_H
#define COLOR_H

enum Color
{
    WHITE, BLACK
};

inline Color operator++ (Color& c) { c = static_cast<Color>(c + 1); return c; };
inline Color operator++ (Color& c, int) { Color old = c; operator++(c); return old; };
inline Color operator-- (Color& c) { c = static_cast<Color>(c - 1); return c; };
inline Color operator-- (Color& c, int) { Color old = c; operator--(c); return old; };
inline Color operator-= (Color& c, int n) { c = static_cast<Color>(c - n); return c; };
inline Color operator!(const Color& c) { return (c == WHITE ? BLACK : WHITE); } 

#endif // COLOR_H