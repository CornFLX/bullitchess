#include "eval.h"

#ifdef DEBUG_EVAL
std::ofstream evallogfile("eval.log");
#endif

EvalHashes eval_hashes;
EvalHashesQueue eval_hashes_queue;

Score Eval::evaluate(Board *board)
{
    auto from_hashes = eval_hashes.find(board->hashkey());
    if (from_hashes != eval_hashes.end()) {
        return from_hashes->second;
    }

    Score score = (material(board) + piece_square(board, board->is_endgame()) + mobility(board)) * (board->side() == WHITE ? 1 : -1);
    eval_hashes[board->hashkey()] = score;
    eval_hashes_queue.push(board->hashkey());
    return score;
}

Score Eval::material(Board *board)
{
    int w_queens  = Bitboards::pop_count(board->piece_bb(W_QUEEN));
    int b_queens  = Bitboards::pop_count(board->piece_bb(B_QUEEN));
    int w_rooks   = Bitboards::pop_count(board->piece_bb(W_ROOK));
    int b_rooks   = Bitboards::pop_count(board->piece_bb(B_ROOK));
    int w_bishops = Bitboards::pop_count(board->piece_bb(W_BISHOP));
    int b_bishops = Bitboards::pop_count(board->piece_bb(B_BISHOP));
    int w_knights = Bitboards::pop_count(board->piece_bb(W_KNIGHT));
    int b_knights = Bitboards::pop_count(board->piece_bb(B_KNIGHT));
    int w_pawns   = Bitboards::pop_count(board->piece_bb(W_PAWN));
    int b_pawns   = Bitboards::pop_count(board->piece_bb(B_PAWN));
    
    int w_pieces = Bitboards::pop_count(board->white_bb());
    int b_pieces = Bitboards::pop_count(board->black_bb());
    
    Bitboard black_sq = 0xaa55aa55aa55aa55;
    Bitboard white_sq = 0x55aa55aa55aa55aa;
    
    if (Bitboards::pop_count(board->occupied_bb()) == 2) return STALEMATESCORE;
    if ( (((w_pieces) == 1 && ( (!b_rooks && !b_knights && !b_pawns && !b_queens) && ((b_bishops == 1) || (b_knights == 1)))))
        || ((b_pieces) == 1 && ( (!w_rooks && !w_knights && !w_pawns && !w_queens) && ((w_bishops == 1) || (w_knights == 1))))) return STALEMATESCORE;
    if (w_pieces == 2 && b_pieces == 2 && w_bishops == 1 && b_bishops == 1 && ( ( (board->piece_bb(B_BISHOP) & black_sq) && (board->piece_bb(W_BISHOP) & black_sq)) || ( (board->piece_bb(B_BISHOP) & white_sq) && (board->piece_bb(W_BISHOP) & white_sq)))) return STALEMATESCORE;
    
    
    Score material_balance = 
          PIECE_VALUE[QUEEN]  * (w_queens - b_queens)
        + PIECE_VALUE[ROOK]   * (w_rooks - b_rooks)
        + PIECE_VALUE[BISHOP] * (w_bishops - b_bishops)
        + PIECE_VALUE[KNIGHT] * (w_knights - b_knights)
        + PIECE_VALUE[PAWN]   * (w_pawns - b_pawns);
                
    if (board->piece_bb(W_BISHOP) & (board->piece_bb(W_BISHOP) - 1)) 
    {
        material_balance += BONUS_BISHOP_PAIR;
    }
    if (board->piece_bb(B_BISHOP) & (board->piece_bb(B_BISHOP) - 1)) 
    {
        material_balance -= BONUS_BISHOP_PAIR;
    }
    
    return material_balance;
}

Score Eval::piece_square(Board *board, bool endgame)
{
    Score score = 0;
    
    Bitboard bb = board->piece_bb(W_PAWN);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        score += W_PAWN_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    bb = board->piece_bb(B_PAWN);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        score -= B_PAWN_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    
    bb = board->piece_bb(W_KNIGHT);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        score += W_KNIGHT_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    bb = board->piece_bb(B_KNIGHT);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        score -= B_KNIGHT_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    
    bb = board->piece_bb(W_BISHOP);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        score += W_BISHOP_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    bb = board->piece_bb(B_BISHOP);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        score -= B_BISHOP_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    
    bb = board->piece_bb(W_ROOK);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        score += W_ROOK_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    bb = board->piece_bb(B_ROOK);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        score -= B_ROOK_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    
    bb = board->piece_bb(W_QUEEN);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        score += W_QUEEN_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    bb = board->piece_bb(B_QUEEN);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        score -= B_QUEEN_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    
    bb = board->piece_bb(W_KING);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        if (endgame)
            score += W_KING_EG_SQUARE_VALUE[sq];
        else
            score += W_KING_MG_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    bb = board->piece_bb(B_KING);
    while (bb)
    {
        Square sq = static_cast<Square>(Bitboards::bsf(bb));
        if (endgame)
            score -= B_KING_EG_SQUARE_VALUE[sq];
        else
            score -= B_KING_MG_SQUARE_VALUE[sq];
        bb ^= (1ULL << sq);
    }
    
    return score;
}

Score Eval::mobility(Board *board)
{
    Score w_mobility = 0;
    Score b_mobility = 0;
    for (PieceType pt = PAWN; pt <= KING; ++pt)
    {
        if (pt == QUEEN) continue;

        w_mobility += MoveGen::piece_mobility(board, pt, WHITE);
        b_mobility += MoveGen::piece_mobility(board, pt, BLACK);
    }
    
    return MOBILITY_WEIGHT*(w_mobility-b_mobility);
}


Score Eval::see(Board *board, Move move)
{
    Value material_gains[32];
    int mg_index = 1;
    
    Square from = Moves::from(move);
    Square target = Moves::to(move);
    Piece piece_from = Moves::piece(move);
    Color stm = Pieces::color_of(piece_from);
    
    material_gains[0] = Material::value(board->square_bb(target));

    Bitboard occupied = board->occupied_bb() ^ BITSET[from];
    Bitboard attackers = board->attackers_to(target, occupied) & occupied;
    
    stm = !stm;
    Bitboard stm_attackers = attackers & board->color_bb(stm);
    if (!stm_attackers) 
    {
        return material_gains[0];
    }

    PieceType captured = (piece_from > W_KING ? static_cast<PieceType>(piece_from-6) : static_cast<PieceType>(piece_from));
    do
    {
        material_gains[mg_index] = -material_gains[mg_index-1] + PIECE_VALUE[captured];
        captured = board->next_attacker(target, attackers, stm_attackers, occupied);

        stm = !stm;
        stm_attackers = attackers & board->color_bb(stm);
        ++mg_index;
    } while (stm_attackers && captured != KING);
    
    while (--mg_index)
    {
        if (material_gains[mg_index] > -material_gains[mg_index-1])
        {
            material_gains[mg_index-1] = -material_gains[mg_index];
        }
    }
    
    return material_gains[0];
}