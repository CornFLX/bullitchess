#include "material.h"

Value PIECE_VALUE[7] = {
    0, 100, 320, 330, 500, 900, 20000
};



Value Material::value(Piece p)
{
    if (p > W_KING)
    {
        p = static_cast<Piece>(p-6);
    }
    return PIECE_VALUE[p];
}
