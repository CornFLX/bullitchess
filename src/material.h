#ifndef MATERIAL_H
#define MATERIAL_H

#include "piece.h"

typedef int Value;

extern Value PIECE_VALUE[7];

namespace Material
{
    /**
     * @brief Returns the piece's value.
     * @param p
     * @return 
     */
    Value value(Piece p);
}

#endif // MATERIAL_H