#ifndef MOVE_H
#define MOVE_H

#include "square.h"
#include "material.h"
#include <string>
#include <iostream>

typedef uint32_t Move;

enum MoveType
{
    NORMAL, CASTLING, ENPASSANT, PROMOTION
};

namespace Moves
{
    // (Square)   From          : 6 bits 1  => 6
    // (Square)   To            : 6 bits 7  => 12
    // (Piece)    Piece         : 4 bits 13 => 16
    // (Piece)    Captured      : 4 bits 17 => 20
    // (Piece)    Promotion     : 4 bits 21 => 24
    // (MoveType) Type          : 2 bits 25 => 26
    
    const Move MOVE_NONE = 28608; // A1 H8 W_KING
    
    inline Move create(Square from, Square to, Piece piece, MoveType type=NORMAL)
    {
        return (from | to << 6 | piece << 12 | type << 24);
    }
    
    inline Move create(Square from, Square to, Piece piece, Piece capture, MoveType type=NORMAL)
    {
        return (from | to << 6 | piece << 12 | capture << 16 | type << 24);
    }
    
    inline Move create(Square from, Square to, Piece piece, Piece capture, Piece promotion, MoveType type=PROMOTION)
    {
        return (from | to << 6 | piece << 12 | capture << 16 | promotion << 20 | type << 24);
    }
    
    inline Square from(Move m)
    {
        return static_cast<Square>(m & 0x0000003f);
    }
    
    inline Square to(Move m)
    {
        return static_cast<Square>((m >> 6) & 0x0000003f);
    }
    
    inline Piece piece(Move m)
    {
        return static_cast<Piece>((m >> 12) & 0x0000000f);
    }
    
    inline Piece capture(Move m)
    {
        return static_cast<Piece>((m >> 16) & 0x0000000f);
    }
    
    inline Piece promotion(Move m)
    {
        return static_cast<Piece>((m >> 20) & 0x0000000f);
    }
    
    inline MoveType type(Move m)
    {
        return static_cast<MoveType>((m >> 24) & 3);
    }
    
    inline bool is_white(Move m)
    {
        return ((m >> 12) & 0x0000000f) < B_PAWN;
    }
    
    inline bool is_black(Move m)
    {
        return ((m >> 12) & 0x0000000f) > W_KING;
    }
    
    inline bool is_capture(Move m)
    {
        return (m & 0x000f0000) != EMPTY;
    }
    
    inline bool is_promotion(Move m)
    {
        return ((m >> 24) & 3) == PROMOTION;
    }
    
    inline bool is_castling(Move m)
    {
        return ((m >> 24) & 3) == CASTLING;
    }
    
    inline bool is_enpassant(Move m)
    {
        return ((m >> 24) & 3) == ENPASSANT;
    }
    
    inline std::string uci(Move m)
    {
        if (m == MOVE_NONE) return "0000";
        
        std::string str = std::string(Squares::str(Moves::from(m)) + Squares::str(Moves::to(m)));
        if (is_promotion(m))
        {
            Piece p = promotion(m);
            str += (p < B_PAWN ? Pieces::str(static_cast<Piece>(p+6)) : Pieces::str(p));
        }
        return str;
    }
}

#endif // MOVE_H