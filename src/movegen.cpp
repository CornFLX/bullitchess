#include "movegen.h"

void MoveGen::add_move(MoveList &ml, Board *board, Move move)
{
    ml.push_back(move);
}

Bitboard MoveGen::east_attack(Bitboard occupied, Square sq)
{
    Bitboard blockers;
    Bitboard east_att;
    Square blocking_square = sq;
    
    east_att = EAST_ATT[sq];
    blockers = east_att & occupied;
    blocking_square = static_cast<Square>(Bitboards::bsf(blockers | 0x8000000000000000ULL));
    east_att ^= EAST_ATT[blocking_square];
    
    return east_att;
}

Bitboard MoveGen::west_attack(Bitboard occupied, Square sq)
{
    Bitboard blockers;
    Bitboard west_att;
    Square blocking_square = sq;
    
    west_att = WEST_ATT[sq];
    blockers = west_att & occupied;
    blocking_square = static_cast<Square>(Bitboards::bsr(blockers | 1ULL));
    west_att ^= WEST_ATT[blocking_square];
    
    return west_att;
}

Bitboard MoveGen::nort_attack(Bitboard occupied, Square sq)
{
    Bitboard blockers;
    Bitboard nort_att;
    Square blocking_square = sq;
    
    nort_att = NORT_ATT[sq];
    blockers = nort_att & occupied;
    blocking_square = static_cast<Square>(Bitboards::bsf(blockers | 0x8000000000000000ULL));
    nort_att ^= NORT_ATT[blocking_square];
    
    return nort_att;
}

Bitboard MoveGen::sout_attack(Bitboard occupied, Square sq)
{
    Bitboard blockers;
    Bitboard sout_att;
    Square blocking_square = sq;
    
    sout_att = SOUT_ATT[sq];
    blockers = sout_att & occupied;
    blocking_square = static_cast<Square>(Bitboards::bsr(blockers | 1ULL));
    sout_att ^= SOUT_ATT[blocking_square];
    
    return sout_att;
}

Bitboard MoveGen::noea_attack(Bitboard occupied, Square sq)
{
    Bitboard blockers;
    Bitboard noea_att;
    Square blocking_square = sq;
    
    noea_att = NOEA_ATT[sq];
    blockers = noea_att & occupied;
    blocking_square = static_cast<Square>(Bitboards::bsf(blockers | 0x8000000000000000ULL));
    noea_att ^= NOEA_ATT[blocking_square];
    
    return noea_att;
}

Bitboard MoveGen::soea_attack(Bitboard occupied, Square sq)
{
    Bitboard blockers;
    Bitboard soea_att;
    Square blocking_square = sq;
    
    soea_att = SOEA_ATT[sq];
    blockers = soea_att & occupied;
    blocking_square = static_cast<Square>(Bitboards::bsr(blockers | 1ULL));
    soea_att ^= SOEA_ATT[blocking_square];
    
    return soea_att;
}

Bitboard MoveGen::sowe_attack(Bitboard occupied, Square sq)
{
    Bitboard blockers;
    Bitboard sowe_att;
    Square blocking_square = sq;
    
    sowe_att = SOWE_ATT[sq];
    blockers = sowe_att & occupied;
    blocking_square = static_cast<Square>(Bitboards::bsr(blockers | 1ULL));
    sowe_att ^= SOWE_ATT[blocking_square];
    
    return sowe_att;
}

Bitboard MoveGen::nowe_attack(Bitboard occupied, Square sq)
{
    Bitboard blockers;
    Bitboard nowe_att;
    Square blocking_square = sq;
    
    nowe_att = NOWE_ATT[sq];
    blockers = nowe_att & occupied;
    blocking_square = static_cast<Square>(Bitboards::bsf(blockers | 0x8000000000000000ULL));
    nowe_att ^= NOWE_ATT[blocking_square];
    
    return nowe_att;
}


Bitboard MoveGen::file_attack(Bitboard occupied, Square sq)
{
    return east_attack(occupied, sq) | west_attack(occupied, sq);
}

Bitboard MoveGen::rank_attack(Bitboard occupied, Square sq)
{
    return nort_attack(occupied, sq) | sout_attack(occupied, sq);
}

int MoveGen::piece_mobility(Board* board, PieceType pt, Color c)
{
    Bitboard occupied, empty, pieces, tos;
    Square from;
    int count = 0;
    
    occupied = board->occupied_bb();
    empty = board->empty_bb();
    
    
    pieces = board->piece_bb(c, pt);
    while (pieces)
    {
        from = static_cast<Square>(Bitboards::bsf(pieces));
        
        tos = 0ULL;
        if (pt == ROOK || pt == QUEEN)
        {
            tos |= (file_attack(occupied, from) | rank_attack(occupied, from));
        }
        if (pt == BISHOP || pt == QUEEN)
        {
            tos |= (noea_attack(occupied, from) | nowe_attack(occupied, from) | soea_attack(occupied, from) | sowe_attack(occupied, from));
        }
        if (pt == KNIGHT)
        {
            tos |= KNIGHT_ATT[from];
        }
        if (pt == PAWN)
        {
            assert(!tos);
            
            tos |= PAWN_PUSH[c][from] & empty;
            if (tos)
            {
                tos |= PAWN_DBLPUSH[c][from];
            }
        }
        if (pt == KING)
        {
            tos |= KING_ATT[from];
        }
        
        tos &= empty;
        
        pieces ^= (1ULL << from);
        count += Bitboards::pop_count(tos);
    }
    
    return count;
}

bool MoveGen::isAttackedBy(Bitboard targets, Board* board, Color c)
{
    Square target;
    Bitboard occupied = board->occupied_bb();
    Color our_color = !c;
    
    while (targets)
    {
        target = static_cast<Square>(Bitboards::bsf(targets));
        
        if (PAWN_ATT[our_color][target] & board->piece_bb(c, PAWN))
        {
            return true;
        }
        
        if (KING_ATT[target] & board->piece_bb(c, KING))
        {
            return true;
        }
        
        if (KNIGHT_ATT[target] & board->piece_bb(c, KNIGHT))
        {
            return true;
        }
        
        Bitboard sliding_att = board->piece_bb(c, QUEEN) | board->piece_bb(c, ROOK);
        if (file_attack(occupied, target) & sliding_att || rank_attack(occupied, target) & sliding_att)
        {
            return true;
        }
        
        sliding_att = board->piece_bb(c, QUEEN) | board->piece_bb(c, BISHOP);
        if (noea_attack(occupied, target) & sliding_att || sowe_attack(occupied, target) & sliding_att)
        {
            return true;
        }
        
        if (nowe_attack(occupied, target) & sliding_att || soea_attack(occupied, target) & sliding_att)
        {
            return true;
        }
        
        targets ^= (1ULL << target);
    }
    
    return false;
}

namespace MoveGen
{
    template <MoveGenType GenType>
    void generate_pawn_moves(MoveList& ml, Board* board, Color c)
    {
        Bitboard pawns = board->piece_bb((c == WHITE ? W_PAWN : B_PAWN));
        Bitboard empty = board->empty_bb();
        Bitboard others = board->color_bb(!c);
        
        Bitboard tos = 0ULL;
        
        Square sqFrom;
        Square sqTo;
        
        Piece captured;
        
        while (pawns)
        {
            sqFrom = static_cast<Square>(Bitboards::bsf(pawns));
            
            if (GenType == ALL)
            {
                tos = PAWN_PUSH[c][sqFrom] & empty;
                if (tos)
                {
                    tos |= PAWN_DBLPUSH[c][sqFrom] & empty;
                }
            }
            tos |= PAWN_ATT[c][sqFrom] & others;
            
            while (tos)
            {
                sqTo = static_cast<Square>(Bitboards::bsf(tos));
                captured = static_cast<Piece>(board->square_bb(sqTo));
                
                if (c == WHITE)
                {
                    if (Squares::rank(sqTo) == RANK_8)
                    {
                        add_move(ml, board, Moves::create(sqFrom, sqTo, W_PAWN, captured, W_QUEEN));
                        add_move(ml, board, Moves::create(sqFrom, sqTo, W_PAWN, captured, W_ROOK));
                        add_move(ml, board, Moves::create(sqFrom, sqTo, W_PAWN, captured, W_BISHOP));
                        add_move(ml, board, Moves::create(sqFrom, sqTo, W_PAWN, captured, W_KNIGHT));
                    }
                    else
                    {
                        add_move(ml, board, Moves::create(sqFrom, sqTo, W_PAWN, captured));
                    }
                }
                else if (c == BLACK)
                {
                    if (Squares::rank(sqTo) == RANK_1)
                    {
                        add_move(ml, board, Moves::create(sqFrom, sqTo, B_PAWN, captured, B_QUEEN));
                        add_move(ml, board, Moves::create(sqFrom, sqTo, B_PAWN, captured, B_ROOK));
                        add_move(ml, board, Moves::create(sqFrom, sqTo, B_PAWN, captured, B_BISHOP));
                        add_move(ml, board, Moves::create(sqFrom, sqTo, B_PAWN, captured, B_KNIGHT));
                    }
                    else
                    {
                        add_move(ml, board, Moves::create(sqFrom, sqTo, B_PAWN, captured));
                    }
                }
                
                tos ^= (1ULL << sqTo);
            }
            
            if (board->enpassant() != SQUARE_NONE && PAWN_ATT[c][sqFrom] & BITSET[board->enpassant()])
            {
                if (c == WHITE)
                {
                    add_move(ml, board, Moves::create(sqFrom, board->enpassant(), W_PAWN, B_PAWN, ENPASSANT));
                }
                else
                {
                    add_move(ml, board, Moves::create(sqFrom, board->enpassant(), B_PAWN, W_PAWN, ENPASSANT));
                }
            }
            
            pawns ^= (1ULL << sqFrom);
        }
    }

    template <MoveGenType GenType>
    void generate_knight_moves(MoveList& ml, Board* board, Color c)
    {
        Piece piece = c == WHITE ? W_KNIGHT : B_KNIGHT;
        Bitboard knights = board->piece_bb(piece);
        Bitboard empty = board->empty_bb();
        Bitboard others = c == WHITE ? board->black_bb() : board->white_bb();
        
        Bitboard tos = 0ULL;
        
        Square sqFrom;
        Square sqTo;
        
        Piece captured;
        
        while (knights)
        {
            sqFrom = static_cast<Square>(Bitboards::bsf(knights));
            if (GenType == CAPTURE)
            {
                tos = KNIGHT_ATT[sqFrom] & others;
            }
            else
            {
                tos = KNIGHT_ATT[sqFrom] & (empty | others);
            }
            
            while (tos) 
            {
                sqTo = static_cast<Square>(Bitboards::bsf(tos));
                captured = static_cast<Piece>(board->square_bb(sqTo));
                
                add_move(ml, board, Moves::create(sqFrom, sqTo, piece, captured));
                
                tos ^= (1ULL << sqTo);
            }
            
            knights ^= (1ULL << sqFrom);
        }
    }

    template <MoveGenType GenType>
    void generate_rook_moves(MoveList& ml, Board* board, Color c)
    {
        Piece piece = c == WHITE ? W_ROOK : B_ROOK;
        
        Bitboard rooks = board->piece_bb(piece);
        Bitboard occupied = board->occupied_bb();
        Bitboard us = board->color_bb(c);
        Bitboard others = board->color_bb(!c);
        
        Bitboard tos = 0ULL;
        
        Square sqFrom;
        Square sqTo;
        
        Piece captured;
        
        while (rooks)
        {
            sqFrom = static_cast<Square>(Bitboards::bsf(rooks));
            
            if (GenType == CAPTURE)
            {
                tos = (file_attack(occupied, sqFrom) | rank_attack(occupied, sqFrom)) & others;
            }
            else
            {
                tos = file_attack(occupied, sqFrom) | rank_attack(occupied, sqFrom);
            }
            
            tos &= ~us;
            
            while (tos)
            {
                sqTo = static_cast<Square>(Bitboards::bsf(tos));
                captured = static_cast<Piece>(board->square_bb(sqTo));
                
                add_move(ml, board, Moves::create(sqFrom, sqTo, piece, captured));
                
                tos ^= (1ULL << sqTo);
            }
            
            rooks ^= (1ULL << sqFrom);
        }
    }

    template <MoveGenType GenType>
    void generate_bishop_moves(MoveList& ml, Board* board, Color c)
    {
        Piece piece = c == WHITE ? W_BISHOP : B_BISHOP;
        
        Bitboard bishops = board->piece_bb(piece);
        Bitboard occupied = board->occupied_bb();
        Bitboard us = board->color_bb(c);
        Bitboard others = board->color_bb(!c);
        
        Bitboard tos = 0ULL;
        
        Square sqFrom;
        Square sqTo;
        
        Piece captured;
        
        while (bishops)
        {
            sqFrom = static_cast<Square>(Bitboards::bsf(bishops));
            
            tos = noea_attack(occupied, sqFrom) | soea_attack(occupied, sqFrom) | sowe_attack(occupied, sqFrom) | nowe_attack(occupied, sqFrom);
            if (GenType == CAPTURE)
            {
                tos &= others;
            }
            
            tos &= ~us;
            
            while (tos)
            {
                sqTo = static_cast<Square>(Bitboards::bsf(tos));
                captured = static_cast<Piece>(board->square_bb(sqTo));
                
                add_move(ml, board, Moves::create(sqFrom, sqTo, piece, captured));
                
                tos ^= (1ULL << sqTo);
            }
            
            bishops ^= (1ULL << sqFrom);
        }
    }

    template <MoveGenType GenType>
    void generate_queen_moves(MoveList& ml, Board* board, Color c)
    {
        Piece piece = c == WHITE ? W_QUEEN : B_QUEEN;
        
        Bitboard queens = (c == WHITE) ? board->piece_bb(W_QUEEN) : board->piece_bb(B_QUEEN);
        Bitboard occupied = board->occupied_bb();
        Bitboard us = board->color_bb(c);
        Bitboard others = board->color_bb(!c);
        
        Bitboard tos = 0ULL;
        
        Square sqFrom;
        Square sqTo;
        
        Piece captured;
        
        while (queens)
        {
            sqFrom =  static_cast<Square>(Bitboards::bsf(queens));
            
            tos = file_attack(occupied, sqFrom) | rank_attack(occupied, sqFrom) | noea_attack(occupied, sqFrom) | soea_attack(occupied, sqFrom) | sowe_attack(occupied, sqFrom) | nowe_attack(occupied, sqFrom);
            if (GenType == CAPTURE)
            {
                tos &= others;
            }
            tos &= ~us;
            
            while (tos)
            {
                sqTo = static_cast<Square>(Bitboards::bsf(tos));
                captured = static_cast<Piece>(board->square_bb(sqTo));
                
                add_move(ml, board, Moves::create(sqFrom, sqTo, piece, captured));
                
                tos ^= (1ULL << sqTo);
            }
            
            queens ^= (1ULL << sqFrom);
        }
    }

    template <MoveGenType GenType>
    void generate_king_moves(MoveList& ml, Board* board, Color c)
    {
        Piece piece = c == WHITE ? W_KING : B_KING;
        
        Bitboard king = board->piece_bb(piece);
        Bitboard empty = board->empty_bb();
        Bitboard others = board->color_bb(!c);
        
        Square sqFrom = static_cast<Square>(Bitboards::bsf(king));
        Square sqTo;
        Piece captured;
        
        Bitboard tos = 0ULL;
        
        if (GenType == CAPTURE)
        {
            tos = KING_ATT[sqFrom] & others;
        }
        else
        {
            tos = KING_ATT[sqFrom] & (empty | others);
        }
        
        while (tos)
        {
            sqTo = static_cast<Square>(Bitboards::bsf(tos));
            captured = static_cast<Piece>(board->square_bb(sqTo));
            
            add_move(ml, board, Moves::create(sqFrom, sqTo, piece, captured));
            
            tos ^= (1ULL << sqTo);
        }
        
        if (c == WHITE && sqFrom == E1)
        {
            if (board->canOO<WHITE>() && ((empty & 0x60) == 0x60) && !isAttackedBy(0x70, board, BLACK) && ((board->piece_bb(W_ROOK) & BITSET[H1]) == BITSET[H1]))
            {
                add_move(ml, board, Moves::create(E1, G1, piece, CASTLING));
            }
            
            if (board->canOOO<WHITE>() && ((empty & 0xe) == 0xe) && !isAttackedBy(0x1c, board, BLACK) && ((board->piece_bb(W_ROOK) & BITSET[A1]) == BITSET[A1]))
            {
                add_move(ml, board, Moves::create(E1, C1, piece, CASTLING));
            }
        }
        else if (c == BLACK && sqFrom == E8)
        {
            if (board->canOO<BLACK>() && ((empty & 0x6000000000000000) == 0x6000000000000000) && !isAttackedBy(0x7000000000000000, board, WHITE) && ((board->piece_bb(B_ROOK) & BITSET[H8]) == BITSET[H8]))
            {
                add_move(ml, board, Moves::create(E8, G8, piece, CASTLING));
            }
            
            if (board->canOOO<BLACK>() && ((empty & 0xe00000000000000) == 0xe00000000000000) && !isAttackedBy(0x1c00000000000000, board, WHITE) && ((board->piece_bb(B_ROOK) & BITSET[A8]) == BITSET[A8]))
            {
                add_move(ml, board, Moves::create(E8, C8, piece, CASTLING));
            }
        }
    }
    
    template void generate_pawn_moves<ALL>(MoveList&, Board*, Color);
    template void generate_pawn_moves<CAPTURE>(MoveList&, Board*, Color);
    template void generate_knight_moves<ALL>(MoveList&, Board*, Color);
    template void generate_knight_moves<CAPTURE>(MoveList&, Board*, Color);
    template void generate_rook_moves<ALL>(MoveList&, Board*, Color);
    template void generate_rook_moves<CAPTURE>(MoveList&, Board*, Color);
    template void generate_bishop_moves<ALL>(MoveList&, Board*, Color);
    template void generate_bishop_moves<CAPTURE>(MoveList&, Board*, Color);
    template void generate_queen_moves<ALL>(MoveList&, Board*, Color);
    template void generate_queen_moves<CAPTURE>(MoveList&, Board*, Color);
    template void generate_king_moves<ALL>(MoveList&, Board*, Color);
    template void generate_king_moves<CAPTURE>(MoveList&, Board*, Color);
}