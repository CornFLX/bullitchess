#ifndef MOVEGEN_H
#define MOVEGEN_H

#include "bitboard.h"
#include "Board.h"
#include "move.h"
#include <vector>

typedef std::vector<Move> MoveList;

enum MoveGenType
{
    ALL, CAPTURE
};

namespace MoveGen
{
    inline void add_move(MoveList& ml, Board* board, Move move);
    
    Bitboard east_attack(Bitboard occupied, Square sq);
    Bitboard west_attack(Bitboard occupied, Square sq);
    Bitboard nort_attack(Bitboard occupied, Square sq);
    Bitboard sout_attack(Bitboard occupied, Square sq);
    
    Bitboard noea_attack(Bitboard occupied, Square sq);
    Bitboard soea_attack(Bitboard occupied, Square sq);
    Bitboard nowe_attack(Bitboard occupied, Square sq);
    Bitboard sowe_attack(Bitboard occupied, Square sq);
    
    Bitboard file_attack(Bitboard occupied, Square sq);
    Bitboard rank_attack(Bitboard occupied, Square sq);
    
    int piece_mobility(Board* board, PieceType pt, Color c);

    bool isAttackedBy(Bitboard targets, Board* board, Color c);

    template <MoveGenType GenType=ALL>
    void generate_pawn_moves(MoveList& ml, Board* board, Color c);
    
    template <MoveGenType GenType=ALL>
    void generate_knight_moves(MoveList& ml, Board* board, Color c);
    
    template <MoveGenType GenType=ALL>
    void generate_rook_moves(MoveList& ml, Board* board, Color c);
    
    template <MoveGenType GenType=ALL>
    void generate_bishop_moves(MoveList& ml, Board* board, Color c);
    
    template <MoveGenType GenType=ALL>
    void generate_queen_moves(MoveList& ml, Board* board, Color c);
    
    template <MoveGenType GenType=ALL>
    void generate_king_moves(MoveList& ml, Board* board, Color c);
    
    template <MoveGenType GenType=ALL>
    inline void generate_moves(MoveList& ml, Board* board, Color c)
    {
        generate_pawn_moves<GenType>(ml, board, c);
        generate_knight_moves<GenType>(ml, board, c);
        generate_rook_moves<GenType>(ml, board, c);
        generate_bishop_moves<GenType>(ml, board, c);
        generate_queen_moves<GenType>(ml, board, c);
        generate_king_moves<GenType>(ml, board, c);
    }
    
    template <MoveGenType GenType=ALL>
    inline void generate_moves(MoveList& ml, Board* board)
    {
        generate_pawn_moves<GenType>(ml, board, board->side());
        generate_knight_moves<GenType>(ml, board, board->side());
        generate_rook_moves<GenType>(ml, board, board->side());
        generate_bishop_moves<GenType>(ml, board, board->side());
        generate_queen_moves<GenType>(ml, board, board->side());
        generate_king_moves<GenType>(ml, board, board->side());
    }
}

#endif // MOVEGEN_H