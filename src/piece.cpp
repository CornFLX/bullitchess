#include <iostream>
#include "piece.h"

PieceType Pieces::make(const char &p)
{
    switch (p)
    {
        case 'P':
        case 'p':
            return PAWN;
            
        case 'R':
        case 'r':
            return ROOK;
            
        case 'N':
        case 'n':
            return KNIGHT;
            
        case 'B':
        case 'b':
            return BISHOP;
            
        case 'Q':
        case 'q':
            return QUEEN;
            
        case 'K':
        case 'k':
            return KING;
            
        default:
            return NO_PIECE;
    }
}

Piece Pieces::make(const char &p, Color c)
{
    if (c == WHITE)
    {
        return static_cast<Piece>(make(p));
    }
    else
    {
        return static_cast<Piece>(make(p)+6);
    }
}

Piece Pieces::make(PieceType pt, Color c)
{
    if (c == WHITE)
    {
        return static_cast<Piece>(pt);
    }
    else
    {
        return static_cast<Piece>(pt+6);
    }
}


std::string Pieces::str(Piece p)
{
    switch (p)
    {
        case W_ROOK:
            return "R";
            break;
            
        case B_ROOK:
            return "r";
            break;
            
        case W_KNIGHT:
            return "N";
            break;
            
        case B_KNIGHT:
            return "n";
            break;
            
        case W_BISHOP:
            return "B";
            break;
            
        case B_BISHOP:
            return "b";
            break;
            
        case W_QUEEN:
            return "Q";
            break;
            
        case B_QUEEN:
            return "q";
            break;
            
        case W_KING:
            return "K";
            break;
            
        case B_KING:
            return "k";
            break;
        
        case W_PAWN:
            return "P";
            break;
            
        case B_PAWN:
            return "p";
            break;
            
        default:
            return "";
            break;
    }
}

std::string Pieces::str(PieceType p)
{
    switch (p)
    {
        case ROOK:
            return "R";
            break;
            
        case KNIGHT:
            return "N";
            break;
            
        case BISHOP:
            return "B";
            break;
            
        case QUEEN:
            return "Q";
            break;
            
        case KING:
            return "K";
            break;
            
        default:
            return "";
            break;
    }
}