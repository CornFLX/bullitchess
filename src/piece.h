#ifndef PIECE_H
#define PIECE_H

#include "color.h"
#include <string>

enum Piece
{
    EMPTY,
    W_PAWN, W_KNIGHT, W_BISHOP, W_ROOK, W_QUEEN, W_KING,
    B_PAWN, B_KNIGHT, B_BISHOP, B_ROOK, B_QUEEN, B_KING
};

inline Piece operator++ (Piece& p) { p = static_cast<Piece>(p + 1); return p; };
inline Piece operator++ (Piece& p, int) { Piece old = p; operator++(p); return old; };

enum PieceType
{
    NO_PIECE, PAWN, KNIGHT, BISHOP, ROOK, QUEEN, KING
};

inline PieceType operator++ (PieceType& p) { p = static_cast<PieceType>(p + 1); return p; };
inline PieceType operator++ (PieceType& p, int) { PieceType old = p; operator++(p); return old; };

namespace Pieces
{
    PieceType make(const char& p);
    Piece make(const char& p, Color c);
    Piece make(PieceType pt, Color c);
    
    /**
     * @brief Return piece's color. (For EMPTY, returns WHITE)
     * @param p
     * @return 
     */
    inline Color color_of(Piece p)
    {
        return (p >= B_PAWN ? BLACK : WHITE);
    }
    
    /**
     * @brief Returns a string representation of the piece.
     * @param p
     * @return 
     */
    std::string str(Piece p);
    
    /**
     * @brief Returns a string representation of the piece.
     * @param p
     * @return 
     */
    std::string str(PieceType p);
}



#endif // PIECE_H