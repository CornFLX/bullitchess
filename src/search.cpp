#include "search.h"
#include "Console.h"

Move Search::think()
{
    SearchPV pv;

    eval_hashes.clear();

    m_stats.reset();
    m_stats.pv = &pv;
    
    m_timeout = false;
    m_tm_check = TIMEOUT_CLOCK_CHECK_INTERVAL;
    m_hashfull_check = TIMEOUT_HASHFULL_CHECK_INTERVAL;
    
    int current_depth = 0;
    while ((++current_depth <= m_maxdepth) || (!m_maxdepth))
    {
        pv.clear();
        m_follow_pv = true;
        
        m_stats.score = pvs(current_depth, -100000, 100000, pv, 0);

        if ((m_stats.score > (CHECKMATESCORE-current_depth)) || (m_stats.score < -(CHECKMATESCORE-current_depth)))
        {
            break;
        }
        
        if (m_timeout) 
        {
            pv = m_stats.last_pv;
            break;
        }
    }

    if (pv.empty()) return Moves::MOVE_NONE;
    return pv.front();
}

Score Search::negamax(int depth, SearchPV &pv, int ply)
{
    if (depth == 0)
    {
        return Eval::evaluate(m_board);
    }
    
    Score best = -100000;
    int moves_found = 0;
    MoveList ml;
    MoveGen::generate_moves(ml, m_board);
    for (Move move : ml)
    {
        m_board->make_move(move);
        if (m_board->position_is_legal())
        {
            ++moves_found;
            ++m_stats.inodes;
            SearchPV child_pv;
            Score score = -negamax(depth-1, child_pv, ply+1);
            if (score > best) 
            {
                best = score;
                pv.clear();
                pv.push_back(move);
                std::copy(child_pv.begin(), child_pv.end(), back_inserter(pv));
                
                if (!ply)
                {
                    m_stats.remember_pv(pv);
                    m_stats.depth = depth;
                    m_stats.score = score;
                    m_stats.display();
                }
            }
        }
        m_board->unmake_move();
    }
    
    if (!moves_found)
    {
        if (m_board->king_is_attacked()) return depth-CHECKMATESCORE;
        else return STALEMATESCORE;
    }
    
    return best;
}

Score Search::alphabeta(int depth, Score alpha, Score beta, SearchPV &pv, int ply)
{
    if (depth == 0)
    {
        return Eval::evaluate(m_board);
    }
    
    int moves_found = 0;
    MoveList ml;
    MoveGen::generate_moves(ml, m_board);
    for (Move move : ml)
    {
        m_board->make_move(move);
        if (m_board->position_is_legal())
        {
            ++moves_found;
            ++m_stats.inodes;
            SearchPV child_pv;
            Score score = -alphabeta(depth-1, -beta, -alpha, child_pv, ply+1);
            
            m_board->unmake_move();
            
            if (score >= beta)
            {
                return beta;
            }
            
            if (score > alpha)
            {
                alpha = score;
                pv.clear();
                pv.push_back(move);
                std::copy(child_pv.begin(), child_pv.end(), back_inserter(pv));
                
                if (!ply)
                {
                    m_stats.remember_pv(pv);
                    m_stats.depth = depth;
                    m_stats.score = score;
                    m_stats.display();
                }
            }
        }
        else 
        {
            m_board->unmake_move();
        }
    }
    
    if (!moves_found)
    {
        if (m_board->king_is_attacked()) return depth-CHECKMATESCORE;
        else return STALEMATESCORE;
    }
    
    return alpha;
}

/*Score Search::pvs(int depth, Score alpha, Score beta, SearchPV &pv, int ply)
{
    if (depth == 0)
    {
        return Eval::evaluate(m_board);
    }
    
    bool pv_found = false;
    
    
    int moves_found = 0;
    MoveList ml;
    MoveGen::generate_all_moves(ml, m_board);
    for (Move move : ml)
    {
        m_board->make_move(move);
        if (m_board->position_is_legal())
        {
            ++moves_found;
            ++m_stats.inodes;
            
            Score score;
            SearchPV child_pv;
            if (pv_found)
            {
                score = -pvs(depth-1, -alpha-1, -alpha, child_pv, ply+1);
                if ((score > alpha) && (score < beta))
                {
                    score = -pvs(depth-1, -beta, -alpha, child_pv, ply+1);
                }
            }
            else
            {
                score = -pvs(depth-1, -beta, -alpha, child_pv, ply+1);
            }
            m_board->unmake_move();
            
            if (score >= beta)
            {
                return beta;
            }
            
            if (score > alpha)
            {
                alpha = score;
                pv.clear();
                pv.push_back(move);
                std::copy(child_pv.begin(), child_pv.end(), back_inserter(pv));
                
                if (!ply)
                {
                    m_stats.remember_pv(pv);
                    m_stats.depth = depth;
                    m_stats.score = score;
                    //m_stats.display();
                    pv_found = true;
                }
            }
        }
        else 
        {
            m_board->unmake_move();
        }
    }
    
    if (!moves_found)
    {
        if (m_board->king_is_attacked()) return depth-CHECKMATESCORE;
        else return STALEMATESCORE;
    }
    
    return alpha;
}*/


Score Search::pvs(int depth, Score alpha, Score beta, SearchPV& pv, int ply)
{
    if (depth == 0) 
    {
        m_follow_pv = false;
        return quiesce(alpha, beta, pv, ply);
    }

    if (m_board->repetition_count() >= 3) return STALEMATESCORE;
    
    int moves_found = 0;
    int pv_moves_found = 0;
    MoveList ml;
    ml.reserve(50);
    MoveGen::generate_moves(ml, m_board);
    Move move;
    while ((move = pick_move(ml, depth)) != Moves::MOVE_NONE)
    {
        m_board->make_move(move);
        if (m_board->position_is_legal())
        {
            ++moves_found;
            ++m_stats.inodes;
            
            if (--m_tm_check <= 0)
            {
                check_clock();
            }

            if (--m_hashfull_check <= 0) {
                check_hashfull();
            }
            
            SearchPV child_pv;
            
            Score score;
            if (pv_moves_found)
            {
                score = -pvs(depth-1, -alpha-1, -alpha, child_pv, ply+1);
                if ((score > alpha) && (score < beta))
                {
                    score = -pvs(depth-1, -beta, -alpha, child_pv, ply+1);
                }
            }
            else
            {
                score = -pvs(depth-1, -beta, -alpha, child_pv, ply+1);
            }
            m_board->unmake_move();
            
            if (m_timeout)
            {
                return 0;
            }
            
            if (score >= beta) 
            {
                m_stats.history[Moves::piece(move)][Moves::to(move)] += depth*depth;
                
                return beta;
            }
            
            if (score > alpha)
            {
                alpha = score;
                ++pv_moves_found;
                
                pv.clear();
                pv.push_back(move);
                std::copy(child_pv.begin(), child_pv.end(), back_inserter(pv));
                
                if (!ply)
                {
                    m_stats.remember_pv(pv);
                    m_stats.depth = depth;
                    m_stats.score = score;
                    m_stats.display();
                }
            }
        } 
        else
        {
            m_board->unmake_move();
        }
    }

    if (pv_moves_found)
    {
        Move pv_move = pv.front();
        m_stats.history[Moves::piece(pv_move)][Moves::to(pv_move)] += depth*depth;
    }

    if (m_board->fifty() >= 100) return STALEMATESCORE;

    if (!moves_found)
    {
        if (m_board->king_is_attacked()) 
        {
            return depth-CHECKMATESCORE+ply-1;
        }
        else return STALEMATESCORE;
    }
    
    return alpha;
}

Score Search::quiesce(Score alpha, Score beta, SearchPV &pv, int ply)
{
    if (m_timeout)
    {
        return 0;
    }
    
    if (m_board->king_is_attacked())
    {
        return pvs(1, alpha, beta, pv, ply);
    }
    
    Score score = Eval::evaluate(m_board);
    if (score >= beta)
    {
        return score;
    }
    
    if (score > alpha)
    {
        alpha = score;
    }
    
    MoveList ml;
    MoveGen::generate_moves<CAPTURE>(ml, m_board);
    for (Move move : ml)
    {
        if (Eval::see(m_board, move) <= 1)
        {
            continue;
        }
        
        m_board->make_move(move);
        if (m_board->position_is_legal())
        {
            ++m_stats.inodes;
            if (--m_tm_check <= 0)
            {
                check_clock();
            }

            if (--m_hashfull_check <= 0) {
                check_hashfull();
            }
            
            SearchPV child_pv;
            score = -quiesce(-beta, -alpha, child_pv, ply+1);
            m_board->unmake_move();
            
            if (score >= beta)
            {
                return score;
            }
            
            if (score > alpha)
            {
                alpha = score;
                pv.clear();
                pv.push_back(move);
                std::copy(child_pv.begin(), child_pv.end(), back_inserter(pv));
            }
        }
        else
        {
            m_board->unmake_move();
        }
    }
    
    return alpha;
}


bool Search::check_clock()
{
    std::string input;
    m_tm_check = TIMEOUT_CLOCK_CHECK_INTERVAL;
    m_hashfull_check = TIMEOUT_HASHFULL_CHECK_INTERVAL;
    while ((input = Console::get_instance().get_input()) != "")
    {
        if (input == "stop")
        {
            m_timeout = true;
            return true;
        }
    }
    
    if (m_tm_enabled && (m_tm_time_per_move <= m_stats.timer.ms()))
    {
        m_timeout = true;
        return true;
    }
    
    return m_timeout;
}

int Search::get_hashfull()
{
    return (1000 * ((sizeof(eval_hashes)*eval_hashes.size()) + (sizeof(eval_hashes_queue)*eval_hashes_queue.size())) / (ucioptions.hash*1024*1024));
}

bool Search::check_hashfull()
{
    int hashfull = get_hashfull();
    std::cout << "info hashfull " << hashfull << std::endl;
    m_stats.hashfull = hashfull;
    if (hashfull >= HASHFULL_BOUND) {

        int to_erase = (eval_hashes_queue.size() * HASHFULL_CLEAR_PERCENT) / 100;
        for (int i = 0; i < to_erase; ++i) {
            eval_hashes.erase(eval_hashes_queue.front());
            eval_hashes_queue.pop();
        }

        return true;
    }

    return false;
}

Move Search::pick_move(MoveList &ml, int depth)
{
    if (m_follow_pv && depth > 1)
    {
        Move pv_move = m_stats.last_pv.front();
        for (MoveList::iterator it = ml.begin() ; it != ml.end() ; ++it)
        {
            if ((*it) == pv_move)
            {
                ml.erase(it);
                return pv_move;
            }
        }
    }
    
    Score best = -1;
    Move best_move = Moves::MOVE_NONE;
    MoveList::iterator to_delete = ml.end();
    for (MoveList::iterator it = ml.begin() ; it != ml.end() ; ++it)
    {
        Move move = (*it);
        Score history_score = m_stats.history[Moves::piece(move)][Moves::to(move)];
        if (history_score > best)
        {
            best = history_score;
            best_move = move;
            to_delete = it;
        }
    }
    
    if (to_delete != ml.end())
    {
        ml.erase(to_delete);
    }
    
    return best_move;
}