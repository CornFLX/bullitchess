#ifndef SEARCH_H
#define SEARCH_H

#include "BullitChess.h"
#include "Board.h"
#include "eval.h"
#include "movegen.h"
#include "move.h"
#include "Timer.h"
#include <vector>
#include <iterator>
#include <algorithm>
#include <array>
#include <cstring>
#include <iomanip>

typedef std::vector<Move> SearchPV;
typedef std::array<std::array<Score, SQUARE_NONE>, B_KING+1> SearchHistory;

struct SearchStats
{
    int depth;
    unsigned int inodes;
    Score score;

    int hashfull;

    SearchPV* pv;
    SearchPV last_pv;
    SearchHistory history;
    Timer timer;
    
    void reset()
    {
        for (Piece p = EMPTY; p <= B_KING; ++p)
        {
            for (Square sq = A1; sq < SQUARE_NONE; ++sq)
            {
                history[p][sq] = 0;
            }
        }
        
        depth = 0;
        inodes = 0;
        score = -100000;
        pv = 0;
        
        timer.reset();
    }
    
    void remember_pv(SearchPV current_pv)
    {
        last_pv = current_pv;
    }
    
    void display()
    {
        double ms = timer.ms();
        std::cout << "info depth " << depth  << " time " << ms << " score cp " << score << " nodes " << inodes << " nps " << (inodes/(ms/1000)) << " hashfull " << hashfull << " pv";
        for (Move move : *pv)
        {
            std::cout << " " << Moves::uci(move);
        }
        
        std::cout << std::endl;
    }
};

class Search
{
private:
    static const int TIMEOUT_CLOCK_CHECK_INTERVAL = 100000;
    static const int TIMEOUT_HASHFULL_CHECK_INTERVAL = 100000;
    static const int HASHFULL_BOUND = 950;
    static const int HASHFULL_CLEAR_PERCENT = 20;
    
    Board* m_board;
    SearchStats m_stats;
    
    bool m_follow_pv;
    bool m_timeout;
    bool m_tm_enabled;

    int m_maxdepth;
    int m_tm_check;
    int m_tm_movestogo;
    int m_tm_inc;
    int m_tm_maxtime;
    
    int m_tm_time_per_move;

    int m_hashfull_check;
    
public:
    Search(Board* board, int maxdepth=0, int tm_movestogo=0, int tm_inc=0, int tm_maxtime=0) : m_board(board), m_maxdepth(maxdepth), m_tm_movestogo(tm_movestogo), m_tm_inc(tm_inc), m_tm_maxtime(tm_maxtime), m_tm_time_per_move(0)
    {
        if (m_tm_maxtime > 0 && m_tm_movestogo > 0)
        {
            m_tm_enabled = true;
            m_tm_time_per_move = (m_tm_maxtime / (m_tm_movestogo+2));
        }
        else if (m_tm_maxtime > 0 && m_tm_inc > 0)
        {
            m_tm_enabled = true;
            m_tm_time_per_move = m_tm_maxtime/40+(m_tm_inc/2);
            if (m_tm_time_per_move >= m_tm_maxtime) m_tm_time_per_move = m_tm_maxtime - 500;
            if (m_tm_time_per_move < 0) m_tm_time_per_move = 100;
        }
        else
        {
            m_tm_enabled = false;
        }
    }
    
    bool timeout() const 
    {
        return m_timeout;
    }
    
    bool check_clock();
    int get_hashfull();
    bool check_hashfull();
    
    Move think();
    Score pvs( int depth, Score alpha, Score beta, SearchPV &pv, int ply);
    Score quiesce( Score alpha, Score beta, SearchPV &pv, int ply );
    Score alphabeta( int depth, Score alpha, Score beta, SearchPV &pv, int ply);
    Score negamax( int depth, SearchPV &pv, int ply);
    
    Move pick_move( MoveList &ml, int depth );
};

#endif // SEARCH_H
