#ifndef SQUARE_H
#define SQUARE_H

#include <string>
#include <iostream>


enum Square
{
    A1, B1, C1, D1, E1, F1, G1, H1,
    A2, B2, C2, D2, E2, F2, G2, H2,
    A3, B3, C3, D3, E3, F3, G3, H3,
    A4, B4, C4, D4, E4, F4, G4, H4,
    A5, B5, C5, D5, E5, F5, G5, H5,
    A6, B6, C6, D6, E6, F6, G6, H6,
    A7, B7, C7, D7, E7, F7, G7, H7,
    A8, B8, C8, D8, E8, F8, G8, H8,
    SQUARE_NONE
};

inline Square operator++ (Square& sq) { sq = static_cast<Square>(sq + 1); return sq; };
inline Square operator++ (Square& sq, int) { Square old = sq; operator++(sq); return old; };
inline Square operator-= (Square& sq, int n) { sq = static_cast<Square>(sq - n); return sq; };


enum File
{
    FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H
};

inline File operator++ (File& f) { f = static_cast<File>(f + 1); return f; };
inline File operator++ (File& f, int) { File old = f; operator++(f); return old; };
inline File operator-- (File& f) { f = static_cast<File>(f - 1); return f; };
inline File operator-- (File& f, int) { File old = f; operator--(f); return old; };
inline File operator-= (File& f, int n) { f = static_cast<File>(f - n); return f; };


enum Rank
{
    RANK_1, RANK_2, RANK_3, RANK_4, RANK_5, RANK_6, RANK_7, RANK_8
};

inline Rank operator++ (Rank& r) { r = static_cast<Rank>(r + 1); return r; };
inline Rank operator++ (Rank& r, int) { Rank old = r; operator++(r); return old; };
inline Rank operator-- (Rank& r) { r = static_cast<Rank>(r - 1); return r; };
inline Rank operator-- (Rank& r, int) { Rank old = r; operator--(r); return old; };
inline Rank operator-= (Rank& r, int n) { r = static_cast<Rank>(r - n); return r; };


namespace Squares
{
    /**
     * @brief Returns the square's file.
     * @param sq
     * @return 
     */
    inline File file(Square sq)
    {
        return File(sq & 7);
    }
    
    /**
     * @brief Returns the square's rank.
     * @param sq
     * @return 
     */
    inline Rank rank(Square sq)
    {
        return Rank(sq >> 3);
    }
    
    /**
     * @brief Returns a string representation of the square (algebraic notation).
     * @param sq
     * @return 
     */
    inline const std::string str(Square sq)
    {
        char s[] = {char('a' + file(sq)), char('1' + rank(sq)), 0};
        return s;
    }
    
    /**
     * @brief Returns the square associated to file and rank parameters.
     * @param f
     * @param r
     * @return 
     */
    inline Square make(File f, Rank r)
    {
        return Square((r << 3) | f);
    }
    
    /**
     * @brief Returns the square from string like "a1"
     * @param str_sq
     * @return 
     */
    inline Square make(std::string str_sq)
    {
        return static_cast<Square>((str_sq.at(0) | 32) + str_sq.at(1) * 8 - 105 - '0' * 8);
    }
}

#endif // SQUARE_H