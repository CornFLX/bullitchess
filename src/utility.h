#ifndef UTILITY_H
#define UTILITY_H

#include <sstream>
#include <string>
#include <vector>

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);

template <class Container>
void delete_them(Container& c)
{
    for (auto& v : c)
    {
        delete v;
    }
    c.clear();
}

#endif // UTILITY_H